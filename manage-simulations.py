#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sem
import argparse
import os.path

ns_path = './'
campaign_dir = ns_path + 'sem'

parser = argparse.ArgumentParser(description='SEM database management script')
parser.add_argument('algo',
                    help='find simulations with this algorithm in the database')
parser.add_argument('-d', '--delete', action='store_true',
                    help='remove found simulations from the database')
args = parser.parse_args()

db_path = os.path.abspath(campaign_dir)
database = sem.DatabaseManager.load(db_path)

param = {
    'algo' : [args.algo]
}

results = database.get_complete_results(param, files_to_load='qos.txt')

good_simulations = 0
for simulation in results:
    if 'qos.txt' in simulation['output']:
        good_simulations = good_simulations + 1
print('found {} simulations with algo={}. {} good.'.format(len(results), args.algo, good_simulations))

if(args.delete):
    print('about to delete found simulations.')
    c = input('continue?(n) ')
    if(c == 'y'):
        print('deleting simulations...')
        for simulation in results:
            database.delete_result(simulation)
        print('finished.')
    else:
        print('aborting.')
