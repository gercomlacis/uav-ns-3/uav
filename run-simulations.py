#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sem
import argparse

ns_path = './'
script = 'simulation'
campaign_dir = ns_path + 'sem'
results_dir = ns_path + 'results'
nRuns = 30

parser = argparse.ArgumentParser(description='SEM script')
parser.add_argument('-e', '--export', action='store_true',
                    help="Don't run simulations. Just export the results.")
parser.add_argument('-o', '--overwrite', action='store_true',
                    help='Overwrite previous campaign.')
args = parser.parse_args()

campaign = sem.CampaignManager.new(ns_path, script, campaign_dir,
            overwrite=args.overwrite, check_repo=False)

param_combinations = {
#	'RngSeed' : 4200,
    'numUAVs' : [4, 8, 12],
    'algo' : ['random', 'kmeans', 'topsis'],
    'numUEs' : [100]
}

if(not args.export):
    print("running simulations with UAVs")
    campaign.run_missing_simulations(sem.list_param_combinations(param_combinations),
                    runs=nRuns, stop_on_errors=False)

result_param = { 
    'algo' : ['random', 'kmeans', 'topsis'],
    'numUAVs' : [4, 8, 12],
    'numUEs' : [100]
}

print("exporting results")
campaign.save_to_folders(result_param, results_dir, nRuns)

param_combinations['numUAVs'] = [0]
param_combinations['algo'] = ['no-uavs']

if(not args.export):
    print("running simulations without UAVs")
    campaign.run_missing_simulations(sem.list_param_combinations(param_combinations),
                    runs=nRuns, stop_on_errors=False)

result_param['numUAVs'] = [0]
result_param['algo'] = ['no-uavs']

print("exporting results")
campaign.save_to_folders(result_param, results_dir, nRuns)
