import sys
import argparse
from pathlib import Path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pyswarms as ps
from sklearn.cluster import KMeans
from topsis import topsis
from pyswarms.utils.plotters.formatters import (Designer, Mesher)
from pyswarms.utils.plotters import plot_contour
from matplotlib.collections import EllipseCollection

# if higher is better =1 else =0 (Distance,remmaining battery)
influence= [0, 1]
pesos = [0.75, 0.25]

def find_outliers(kmeans, positions, max_radius):
    outlier_count = 0
    #get cluster id of each position
    labels = kmeans.labels_
    #get distance matrix
    t = kmeans.transform(positions)
    for user in range(len(labels)):
        cluster = labels[user]
        distance_to_center = t[user][cluster]
        if distance_to_center > max_radius:
            labels[user] = -1
            outlier_count = outlier_count + 1
    return outlier_count

def plot_clusters(kmeans_centers, pso_centers, labels, df, cells, radius):
    u_labels = np.unique(labels)[1:]
    plt.clf()
    for i in u_labels:
        plt.scatter(df['x'][labels == i], df['y'][labels == i] , label = i)

    MCs = cells[cells['type'] == 'MC']
    plt.scatter(MCs['x'], MCs['y'], label = 'MCs', color='blue', edgecolors='black')

    SCs = cells[cells['type'] == 'SC']
    plt.scatter(SCs['x'], SCs['y'], label = 'SCs', color='lightblue', edgecolors='black')

    plt.scatter(df['x'][labels == -1], df['y'][labels == -1] , label = 'outliers',
            color='white', edgecolors='black')
    plt.scatter(kmeans_centers['x'], kmeans_centers['y'], label = 'kmeans centers',
            color='red', marker='x')
    if len(pso_centers) > 0:
        plt.scatter(pso_centers['x'], pso_centers['y'], label = 'pso centers',
                color='green', marker='x')

    ax = plt.gca()
    ec = EllipseCollection(2*radius, 2*radius, 0, units='xy', offsets=SCs[['x','y']], 
                           offset_transform=ax.transData, facecolor='#00000000', edgecolor='lightblue')
    ax.add_collection(ec)
    ec = EllipseCollection(2*radius, 2*radius, 0, units='xy', offsets=kmeans_centers,
                           offset_transform=ax.transData, facecolor='#00000000', edgecolor='red')
    ax.add_collection(ec)

    plt.legend()
    plt.show()

def find_clusters(positions, max_radius, max_users, max_clusters):
    positions = positions.copy()
    num_users = len(positions)
    clusters = min(int(np.ceil(num_users/max_users)), max_clusters)

    kmeans = KMeans(n_clusters=clusters, n_init='auto', random_state=42).fit(positions)
    outlier_count = find_outliers(kmeans, positions, max_radius)
    print(f"clusters: {clusters} outliers: {outlier_count}")
    labels = kmeans.labels_

    #cluster without outliers
    kmeans = KMeans(n_clusters=clusters, n_init='auto', random_state=42).fit(positions[labels != -1])
    positions['labels'] = -1
    positions.loc[labels != -1, 'labels'] = kmeans.labels_
    return kmeans.cluster_centers_, positions['labels']

def calc_distances(point, positions):
    #numpy.linalg.norm(a - b) is equivalent to the euclidean distance
    #between the vectors a and b
    sub = point - positions
    distances = sub.apply(np.linalg.norm, axis=1)
    return distances

def position_score(uav_pos, users_pos, weights):
    distances = calc_distances(uav_pos, users_pos)
    distances = distances * weights
    score = np.linalg.norm([distances.mean(), distances.std()])
    return score

def g(x, users):
    """Higher-level method to calculate the score for the whole swarm.

    Inputs
    ------
    x: numpy.ndarray of shape (n_particles, dimensions)
        The swarm that will perform the search

    Returns
    -------
    numpy.ndarray of shape (n_particles, )
        The computed score for each particle
    """
    n_particles = x.shape[0]

    users_pos = users[['x', 'y']]
    weights = users['priority']
    j = [position_score(x[i], users_pos, weights) for i in range(n_particles)]
    return np.array(j)

def run_pso(n_clusters, user_data, animate):
    # Set-up hyperparameters
    swarm_options = {'c1': 0.5, 'c2': 0.3, 'w':0.6}

    pso_centers = []
    for cluster_index in range(n_clusters):
        cluster = user_data[user_data['cluster'] == cluster_index]
        if len(cluster) == 1:
            pso_centers.append(cluster[['x','y']].iloc[0].to_numpy())
            continue

        lower_bound = [cluster['x'].min(), cluster['y'].min()]
        upper_bound = [cluster['x'].max(), cluster['y'].max()]

        # Call instance of GlobalBestPSO
        optimizer = ps.single.GlobalBestPSO(n_particles=10, dimensions=2,
                                            options=swarm_options,
                                            bounds=(lower_bound, upper_bound))
        # Perform optimization
        cost, pos = optimizer.optimize(g, iters=30, users=cluster)
        pso_centers.append(pos)

        if not animate:
            continue
        # Make animation
        des = Designer(limits=[(lower_bound[0], upper_bound[0]),
            (lower_bound[1], upper_bound[1]), (0, 1000)])
        mesher = Mesher(func=lambda x: g(x, cluster), delta=2,
                limits=[(lower_bound[0], upper_bound[0]),
                        (lower_bound[1],upper_bound[1])],
                levels=12)
        animation = plot_contour(pos_history=optimizer.pos_history,
                designer=des, mesher=mesher)
        animation.save(f"plot{cluster_index}.mp4", fps=10)
    pso_centers = pd.DataFrame(pso_centers, columns=['x','y'])
    return pso_centers

def format_result(uav_id, new_position, users):
    new_position_str = [str(coord) for coord in new_position]
    new_position_str = " ".join(new_position_str)

    users_str = [str(user) for user in users]
    users_str = " ".join(users_str)

    return f"{uav_id};{new_position_str};{users_str}"

def check_closest(index, point, coords):
    distances = calc_distances(point, coords)
    closest = distances.values.argmin()
    new_index = coords.index[closest]
    return new_index == index

def reorder_clusters(centers):
    p = Path("centers.txt")
    if not p.exists():
        return

    f = open(p)
    lines = f.readlines()
    values = [line.split(';')[1] for line in lines]
    old_centers = [val.split() for val in values]
    old_centers = [(float(c[0]), float(c[1])) for c in old_centers]
    old_centers = pd.DataFrame(old_centers, columns=['x','y'])

    matched = []
    available = [True] * len(old_centers)
    new_index = len(old_centers)
    for row in centers.itertuples():
        center = [row.x, row.y]
        temp_available = available.copy()
        while len(old_centers[temp_available]) > 0:
            available_old_centers = old_centers[temp_available]
            distances = calc_distances(center, available_old_centers)
            closest = distances.values.argmin()
            old_index = available_old_centers.index[closest]
            if check_closest(row.Index,
                    available_old_centers.iloc[closest], centers[row.Index:]):
                matched.append(old_index)
                available[old_index] = False
                break
            else:
                temp_available[old_index] = False
        else:
            matched.append(new_index)
            new_index = new_index + 1
    centers.index = pd.Index(matched)
    centers.sort_index(inplace=True)
    centers.reset_index(drop=True, inplace=True)

def main():
    parser = argparse.ArgumentParser(description='''UAV positioning algorithm
        using k-means for user clusterization, PSO for optimum positioning
        within clusters and TOPSIS for UAV selection''')
    parser.add_argument('--cluster-radius', type=int, default=100)
    parser.add_argument('--max-users', type=int, default=10)
    parser.add_argument('--enable-pso', action='store_true')
    parser.add_argument('--enable-topsis', action='store_true')
    parser.add_argument('--animate', action='store_true')
    parser.add_argument('--plot-clusters', action='store_true')
    args = parser.parse_args()
    
    user_data = pd.read_csv("user_status.txt", sep=' ',
        names=['id','x', 'y', 'z', 'priority'])
    if user_data.empty:
        return

    uav_data = pd.read_csv("uav-status.txt", sep=' ',
        names=['time', 'id', 'x', 'y', 'energy'])
    if uav_data.empty:
        return
    current_time = uav_data.iloc[-1]['time']
    uav_data = uav_data[uav_data['time'] == current_time]
    n_uavs = len(uav_data)

    if args.plot_clusters:
        cell_data = pd.read_csv("cells.txt", sep=',', header=0)

    kmeans_centers, clusters = find_clusters(user_data[['x', 'y']],
            args.cluster_radius, args.max_users, n_uavs)
    kmeans_centers = pd.DataFrame(kmeans_centers, columns=['x','y'])
    user_data['cluster'] = clusters

    n_clusters = clusters.max() + 1
    pso_centers = []
    if args.enable_pso:
        pso_centers = run_pso(n_clusters, user_data, args.animate)
        centers = pso_centers
    else:
        centers = kmeans_centers

    if args.plot_clusters:
        plot_clusters(kmeans_centers, pso_centers, clusters, user_data,
                      cell_data, args.cluster_radius)

    available_id = [True] * n_uavs
    results = []
    reorder_clusters(centers)
    for row in centers.itertuples():
        center = [row.x, row.y]
        available = uav_data[available_id]
        if len(available) == 1:
            best_id = available['id'].iloc[0]
        else:
            uav_pos = available[['x','y']]
            distances = calc_distances(center, uav_pos)
            if args.enable_topsis:
                inputs = {'distances': distances, 'energy': available['energy']}
                inputs = pd.DataFrame.from_dict(inputs)
                decision = topsis(inputs, pesos, influence)
                #print (topsis(inputs, pesos, influence))
                decision.calc()
                best = decision.optimum_choice
                #print  (decision.optimum_choice)
            else:
                best = distances.values.argmin()
            best_id = int(available['id'].iloc[best])
        users = user_data.loc[user_data['cluster'] == row.Index, 'id'].tolist()
        available_id[best_id] = False
        results.append(format_result(best_id, center, users))

    with open("centers.txt", "w") as c:
        for res in results:
            c.write(f"{res}\n")
if __name__ == "__main__":
    main()
