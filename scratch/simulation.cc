#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <limits>
#include <list>
#include <map>
#include <memory>
#include <random>
#include <sstream>
#include <string>
#include <time.h> /* time */
#include <unordered_map>
#include <utility> // std::pair
#include <vector>

#include <boost/algorithm/string/classification.hpp> // Include boost::for is_any_of
#include <boost/algorithm/string/split.hpp>          // Include for boost::split

#include "ns3/core-module.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/ipv4-address.h"
#include "ns3/log.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/applications-module.h"
#include "ns3/lte-module.h"
#include <ns3/buildings-helper.h>
#include "ns3/mobility-module.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/netanim-module.h"

#include "ns3/basic-energy-source.h"
#include "ns3/basic-energy-source-helper.h"
#include "ns3/energy-source-container.h"

#include "ns3/evalvid-client-server-helper.h"
#include "ns3/evalvid-client.h"
#include "ns3/evalvid-server.h"

#include "matriz.h"
#include "ns3/propagation-environment.h"
#include "ns3/vector.h"

using namespace ns3;

std::default_random_engine generator;

NS_LOG_COMPONENT_DEFINE("uav-edge");

enum class EnergyType { mobility, comms, compute};

class UAVEnergyTrace {
public:
	std::ofstream mobility;
	std::ofstream comms;
	std::ofstream compute;

	UAVEnergyTrace()
	{
		mobility.open("mobility-energy.txt", std::ofstream::out | std::ofstream::trunc);
		comms.open("comms-energy.txt", std::ofstream::out | std::ofstream::trunc);
		compute.open("compute-energy.txt", std::ofstream::out | std::ofstream::trunc);
	}

	~UAVEnergyTrace()
	{
		mobility.close();
		comms.close();
		compute.close();
	}
};

class UAVStatus {
public:
	static std::ofstream mobility_log;
	static std::ofstream status;
	double movement_start {0};
	UAVStatus(){}
};


std::ofstream UAVStatus::mobility_log {
	"uav-mobility.txt", std::ofstream::out | std::ofstream::trunc
};
std::ofstream UAVStatus::status {
	"uav-status.txt", std::ofstream::out | std::ofstream::trunc
};

// struct that contains info a  about the handovers performed in the network
struct Handover
{
  double time;
  int user;
  int rnti;
  int source;
  int target;

  Handover(double t, int u, int r, int s, int tg)
      : time{t}, user{u}, rnti{r}, source{s}, target{tg} {}

  // operator to compare two handover instances within a given time window
  bool operator==(const Handover &other) const
  {
    return std::abs(other.time - time) < 2 && user == other.user;
    // return user == other.user  && time - other.time > 0;
    //&& source == other.source && source == other.source &&
    //    target == other.target;
  }

  friend std::ostream &operator<<(std::ostream &os, const Handover &h)
  {
    os << "Handover(" << h.time << ", " << h.user << ", " << h.rnti << ", " << h.source << ", "
       << h.target << ")";

    return os;
  }
};

struct Cluster {
	int uav_id;
	Vector new_position;
	std::list<int> users;
};

class Task {
	public:
		static const Task AR;
		static const Task VR;
		static const Task VoD;

	private:
		Task(std::string name, double delay, double size, short priority)
			: mName(name)
			, mDelay(delay)
			, mSize(size)
			, mPriority(priority)
		{}

		std::string mName;
		double mDelay;
		double mSize;
		short mPriority; //A higher number indicates a higher priority

	public:
		Task() {
			mName = "";
			mDelay = 0;
			mSize = 0;
			mPriority = 0;
		}

		std::string get_name() const { return mName; }
		double get_delay() const { return mDelay; }
		double size_bits() const { return mSize * 1000000; }
		double size_bytes() const { return size_bits() / 8; }
		double size_mbits() const { return mSize; }
		double size_mbytes() const { return mSize/8; }
		short get_priority() const { return mPriority; }

		friend std::ostream &operator<<(std::ostream &os, const Task &t)
		{
			os << "Task(name=" << t.mName << ", delay=" << t.mDelay << ", size=" << t.mSize << ")";
			return os;
		}
};

class CellId {
	int value;

	CellId(int val) {
		NS_ASSERT(val >= 1);
		value = val;
	}

public:
	static CellId fromRaw(int raw_id) {
		return CellId(raw_id);
	}

	static CellId fromZeroBasedId(int id) {
		return CellId(id+1);
	}

	int getValue() { return value; }
};

const Task Task::AR("AR", 0.013, 97.0, 3);
const Task Task::VR("VR", 0.013, 25.0, 2);
const Task Task::VoD("VoD", 0.25, 10.0, 1);

enum class NodeType {STATIC, GBS, UAV, UE};

const double beta_i0 = 1;
const double Ci = 2.0e8;
const double fiUAV = 1.5e9;

const float MOBILITY_ENERGY_INTERVAL = 1; //second
const float COMMS_ENERGY_INTERVAL = 1; //seconds
const float COMPUTE_ENERGY_INTERVAL = 1; //seconds
const float COMMS_ENERGY_COST = 50; //Joules
const float COMPUTE_ENERGY_COST = 30; //Joules
const float ZERO_SIGNAL_LEVEL = -999; //dbm
const float CONNECTED_SIGNAL_LEVEL = -120; //dbm
const float ENB_HEIGHT = 30; //m
const float UAV_HEIGHT = 20; //m
const unsigned int GBS_MAX_USERS = 15;
const unsigned int UAV_MAX_USERS = 8;
const unsigned int ENB_BW = 100;
const unsigned int UAV_BW = 100;

uint32_t numMacroCells = 4;
uint32_t numSmallCells = 8;
uint32_t numUAVs = 8;
uint32_t numUEs = 100;
uint8_t num_ccs; // initialized in main()
uint32_t mimo_mode = 2;
uint32_t cluster_radius = 250;
// these variables must be initialized in main()
//---------------------------//
uint32_t numBSs;
uint32_t numCells;
uint32_t numEdgeServers;
uint32_t number_of_hot_spots;
//---------------------------//
double MCtxpower = 40;
double SCtxpower = 20;
double UAVtxpower = 20;
double OffTxPower = -20;
float uav_speed = 13;
std::string ns3_dir;
UAVEnergyTrace uav_energy_trace;
std::vector<UAVStatus> uav_status;
unordered_map<unsigned int, Ptr<Node>> uav_by_id;
//sets of ues served by each uav
std::vector<std::set<int>> uav_connected_ues;
//sets of ues served by each ground base station
std::vector<std::set<int>> gbs_connected_ues;

// scenario variables
std::vector<double> bitrates = {1000000,1000000,100000};
std::vector<double> ues_sinr;
std::ofstream ues_sinr_file;
std::vector<double> time_to_centroid;
std::ofstream time_to_centroid_file;
std::ofstream ue_positions_log;
uint32_t active_drones = 0;
// std::string clustering_algoritm = "kmeans";

Time management_interval = Seconds(10);

std::string mobil_trace = "traces/koln.tcl";
std::string req_mode = "random";
std::string requests_trace = "traces/requests.tcl_but_not_really";
std::string handover_policy = "classic";
float distance_multiplier = 1.0 / 10;

uint16_t node_remote = 1; // HOST_REMOTO
// double cell_thr_average = 0; // average throughput of used cells

/* ================= control variables ==================*/
bool disableDl = false;
bool disableUl = false;
bool enablePrediction = true;
bool verbose = false;
bool enableHandover = true;
bool useCa = true;

/*============= state variables =======================*/
/* connection management structures */
matriz<int> cellUe;
std::vector<std::vector<double>> neighbors;
std::vector<bool> handover_pending;
std::vector<int> guided_target_cell;
// structure to store handover predictions
// // [0] -> time of the handover
// // [1] -> source cell
// // [2] -> target cell
matriz<int> handoverPredictions;
std::vector<double> cell_usage;
std::vector<double> cell_throughput;

std::vector<Ipv4Address> user_ip;
std::vector<Task> user_requests;
std::vector<double> user_delay;
std::vector<double> user_jitter;
std::vector<double> user_throughput;
std::vector<double> user_pdr;
std::vector<bool> unserved_users;
std::unordered_map<uint16_t, Task> port2Task;

std::vector<std::vector<int>> edgeUe;

std::map<int, int> handovers_per_second;

// peek results at the end of the simulation
std::vector<double> peek_user_delay;
std::vector<double> peek_service_level;
std::vector<double> peek_uav_usage;

// store historical handover info
std::vector<Handover> handover_vector;
unsigned int handNumber = 0; // number of handovers executed

std::vector<bool> drones_in_use(numUAVs);
std::vector<bool> hot_spots_served(number_of_hot_spots);

std::map<int, std::map<int, int>> rnti_cells;

// mapping nodelist ids to actual imsis
std::map<int, int> path_imsi;

// /*MIGRATION VARIABLES*/
// enable logs
// perform migrations
Time managerInterval = Seconds(1);
std::string algorithm = "topsis";

// server characteristics
// the first index is the metric: lat, bw, and cost
// second index is the type of server: mist, edge, fog, cloud
int serverReqs[4][3] = {{1, 1, 4}, {1, 2, 3}, {10, 10, 2}, {100, 100, 1}};

// applications class 1, 2, 3, and 4
// latency in ms and bw in mbps, and prioritary
int applicationReqs[3][3] = {{1, 10, 1}, {10, 100, 1}, {1000, 1, 0}};
uint16_t applicationType = 0;

//-----VARIABLES THAT DEPEND ON THE NUMBER OF SERVERS----
// The resources variable tells which server has one or
// more of the recources needed in this simulation
// the resources are:
vector<uint16_t> resources;

// type of cell position allocation
bool rowTopology = false;
bool randomCellAlloc = true;

// ----------VARIABLES TO CALCULATE METRICS-----------
std::vector<int> latency;
/* END OF MIGRATION VARIABLES */

std::vector<int> cost;
// node containers as global objects to make mobility management possible
NodeContainer uavNodes;
NodeContainer ueNodes;
NodeContainer BSNodes;
NodeContainer GBSNodes;
NodeContainer mcNodes;
NodeContainer scNodes;

NetDeviceContainer enbDevs;
NetDeviceContainer ueDevs;

/*====== REQUIRED PROTOTYPES =======*/
int get_cell(int);
int get_cell_from_imsi(int);
int get_closest_center_index(Ptr<Node>, std::vector<std::pair<int, int>>);
Vector get_node_position(Ptr<Node>);
void handoverManager(std::string);
int getCellId(int);
Ptr<ListPositionAllocator> generatePositionAllocator(int, int, float, std::string allocation);
static int get_relative_id(int absolute_id, NodeType type);
std::vector<Cluster> topsis(NodeContainer users);
static void renew_task(Ptr<Node> user, Task task);
std::vector<Cluster> parse_result_file();
void CourseChange(std::string, Ptr<const MobilityModel>);

// global lte helper for handover management
Ptr<LteHelper> lteHelper = CreateObject<LteHelper>();

Ptr<Node> remoteHost;

void initialize_vectors()
{
	/*============= state variables =======================*/
	/* connection management structures */
	cellUe.setDimensions(numCells, numUEs);
	handover_pending.assign(numUEs, false);
	guided_target_cell.assign(numUEs, -1);
	neighbors.assign(numCells, std::vector<double>(numUEs, ZERO_SIGNAL_LEVEL));
	handoverPredictions.setDimensions(numUEs, 3);

	cell_usage.assign(numCells, 0);
	cell_throughput.assign(numCells, 0);

	user_ip.resize(numUEs);
	user_requests.resize(numUEs);
	user_delay.assign(numUEs, 0);
	user_jitter.assign(numUEs, 0);
	user_throughput.assign(numUEs, 0);
	user_pdr.assign(numUEs, 100);
	unserved_users.assign(numUEs, false);
	uav_connected_ues.assign(numUAVs, std::set<int>());
	gbs_connected_ues.assign(numMacroCells+numSmallCells, std::set<int>());

	edgeUe.assign(numEdgeServers, std::vector<int>(numUEs, 0));

	drones_in_use.resize(numUAVs);
	uav_status.resize(numUAVs);
	hot_spots_served.resize(number_of_hot_spots);

	//-----VARIABLES THAT DEPEND ON THE NUMBER OF SERVERS----
	// The resources variable tells which server has one or
	// more of the recources needed in this simulation
	// the resources are:
	resources.assign(numEdgeServers, 10);
}

bool IsTopLevelSourceDir (std::string path)
{
	bool haveVersion = false;
	bool haveLicense = false;

	//
	// If there's a file named VERSION and a file named LICENSE in this
	// directory, we assume it's our top level source directory.
	//

	std::list<std::string> files = SystemPath::ReadFiles (path);
	for (std::list<std::string>::const_iterator i = files.begin (); i != files.end (); ++i)
	{
		if (*i == "VERSION")
		{
			haveVersion = true;
		}
		else if (*i == "LICENSE")
		{
			haveLicense = true;
		}
	}

	return haveVersion && haveLicense;
}

std::string GetTopLevelSourceDir (void)
{
	std::string self = SystemPath::FindSelfDirectory ();
	std::list<std::string> elements = SystemPath::Split (self);
	while (!elements.empty ())
	{
		std::string path = SystemPath::Join (elements.begin (), elements.end ());
		if (IsTopLevelSourceDir (path))
		{
			return path + "/";
		}
		elements.pop_back ();
	}
	NS_FATAL_ERROR ("Could not find source directory from self=" << self);
}

Ptr<Node> config_LTE(Ptr<LteHelper> lte, Ptr<PointToPointEpcHelper> epc)
{
	lte->SetEpcHelper(epc);

	lte->SetAttribute ("UseCa", BooleanValue (useCa));
	if(useCa){
		lte->SetAttribute ("NumberOfComponentCarriers", UintegerValue (num_ccs));
		lte->SetAttribute ("EnbComponentCarrierManager", StringValue ("ns3::RrComponentCarrierManager"));
	}

	//Discard sinr trace. It can generate files bigger than 1GB.
	Config::SetDefault("ns3::PhyStatsCalculator::DlRsrpSinrFilename", StringValue("/dev/null"));
	Config::SetDefault("ns3::PhyStatsCalculator::UlSinrFilename", StringValue("/dev/null"));
	Config::SetDefault("ns3::MacStatsCalculator::DlOutputFilename", StringValue("/dev/null"));
	Config::SetDefault("ns3::PhyRxStatsCalculator::DlRxOutputFilename", StringValue("/dev/null"));
	Config::SetDefault("ns3::PhyTxStatsCalculator::DlTxOutputFilename", StringValue("/dev/null"));
	Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity", UintegerValue(320));

	NS_LOG_UNCOND("Pathloss model: HybridBuildingsPropagationLossModel ");
	Config::SetDefault("ns3::ItuR1411NlosOverRooftopPropagationLossModel::StreetsOrientation", DoubleValue (30));
	Config::SetDefault("ns3::ItuR1411NlosOverRooftopPropagationLossModel::BuildingsExtend", DoubleValue (1000));
	lte->SetAttribute ("PathlossModel", StringValue ("ns3::HybridBuildingsPropagationLossModel"));
	lte->SetPathlossModelAttribute ("ShadowSigmaOutdoor", DoubleValue (0));
	lte->SetPathlossModelAttribute ("Los2NlosThr", DoubleValue (250));
	lte->SetPathlossModelAttribute ("RooftopLevel", DoubleValue (5));

	Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (true));
	Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (true));
	lte->SetAttribute ("UseIdealRrc", BooleanValue (true));
	lte->SetAttribute ("UsePdschForCqiGeneration", BooleanValue (true));

	//Uplink Power Control
	Config::SetDefault ("ns3::LteUePhy::EnableUplinkPowerControl", BooleanValue (true));
	Config::SetDefault ("ns3::LteUePowerControl::ClosedLoop", BooleanValue (true));
	//ns3::LteFfrDistributedAlgorithm works with Absolute Mode Uplink Power Control
	Config::SetDefault ("ns3::LteUePowerControl::AccumulationEnabled", BooleanValue (false));

	lteHelper->SetSchedulerType ("ns3::PfFfMacScheduler");
	lteHelper->SetSchedulerAttribute("UlCqiFilter", EnumValue(FfMacScheduler::PUSCH_UL_CQI));

	lteHelper->SetFfrAlgorithmType("ns3::LteFrSoftAlgorithm");
	lteHelper->SetFfrAlgorithmAttribute("AllowCenterUeUseEdgeSubBand", BooleanValue(false));
	lteHelper->SetFfrAlgorithmAttribute("RsrqThreshold", UintegerValue(25));
	lteHelper->SetFfrAlgorithmAttribute("CenterPowerOffset",
										UintegerValue(LteRrcSap::PdschConfigDedicated::dB_6));
	lteHelper->SetFfrAlgorithmAttribute("EdgePowerOffset",
										UintegerValue(LteRrcSap::PdschConfigDedicated::dB3));
	lteHelper->SetFfrAlgorithmAttribute("CenterAreaTpc", UintegerValue(0));
	lteHelper->SetFfrAlgorithmAttribute("EdgeAreaTpc", UintegerValue(3));

	if (handover_policy == "none")
		lteHelper->SetHandoverAlgorithmType("ns3::A2A4RsrqHandoverAlgorithm");
	else
		lteHelper->SetHandoverAlgorithmType("ns3::NoOpHandoverAlgorithm");

	Config::SetDefault("ns3::LteEnbPhy::NoiseFigure", DoubleValue(9)); // Default 5
	// Modo de transmissão (SISO [0], Diversity [1], MIMO [2])
	Config::SetDefault("ns3::LteEnbRrc::DefaultTransmissionMode", UintegerValue(mimo_mode));
	return epc->GetPgwNode();
}

void install_LTE(Ptr<LteHelper> lteHelper, Ptr<PointToPointEpcHelper> epcHelper, NodeContainer mcNodes, NodeContainer scNodes, NodeContainer uavNodes, NodeContainer UEs)
{
	NetDeviceContainer staticEnbDevs;
	std::vector<int> mc_frequency_id = {1,1,2,2};
	std::vector<int> sc_frequency_id = {1,1,2,2,1,1,1,1};
	std::vector<int> uav_frequency_id = {3,3,3,3,
										2,2,1,1,
										3,3,3,3};

	Config::SetDefault("ns3::LteEnbPhy::TxPower", DoubleValue(MCtxpower));
	lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (100));
	lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (100+18000));
	lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (ENB_BW)); //Set Download BandWidth
	lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (ENB_BW)); //Set Upload Bandwidth
	for (uint32_t i = 0; i < numMacroCells; ++i) {
		lteHelper->SetFfrAlgorithmAttribute(
				"FrCellTypeId", UintegerValue(mc_frequency_id[i]));
		staticEnbDevs.Add(lteHelper->InstallEnbDevice(mcNodes.Get(i)));
	}

	Config::SetDefault("ns3::LteEnbPhy::TxPower", DoubleValue(SCtxpower));
	for (uint32_t i = 0; i < numSmallCells; ++i) {
		lteHelper->SetFfrAlgorithmAttribute(
				"FrCellTypeId", UintegerValue(sc_frequency_id[i]));
		staticEnbDevs.Add(lteHelper->InstallEnbDevice(scNodes.Get(i)));
	}
	enbDevs.Add(staticEnbDevs);
	
	//UAVs cells start powered off
	Config::SetDefault("ns3::LteEnbPhy::TxPower", DoubleValue(OffTxPower));
	lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (UAV_BW)); //Set Download BandWidth
	lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (UAV_BW)); //Set Upload Bandwidth
	for (uint32_t i = 0; i < numUAVs; ++i) {
		lteHelper->SetFfrAlgorithmAttribute(
				"FrCellTypeId", UintegerValue(uav_frequency_id[i]));
		enbDevs.Add(lteHelper->InstallEnbDevice(uavNodes.Get(i)));
	}

	lteHelper->AddX2Interface(BSNodes);

	ueDevs = lteHelper->InstallUeDevice(UEs);

	Ipv4InterfaceContainer ueIpIface;
	ueIpIface = epcHelper->AssignUeIpv4Address(NetDeviceContainer(ueDevs));
	if(numMacroCells + numSmallCells > 0)
		lteHelper->AttachToClosestEnb(ueDevs, staticEnbDevs);
	else
		lteHelper->AttachToClosestEnb(ueDevs, enbDevs);
}

void move_uav(Ptr<Node> uav, Vector destination, double n_vel);

void install_mobility(NodeContainer staticNodes, NodeContainer mcNodes, NodeContainer scNodes, NodeContainer uavNodes, NodeContainer UEs)
{
	Ptr<ListPositionAllocator> allocator = CreateObject<ListPositionAllocator> ();
	allocator->Add (Vector(500, 500, 0));
	allocator->Add (Vector(500, 520, 0));
	allocator->Add (Vector(500, 540, 0));

	MobilityHelper staticNodesHelper;
	staticNodesHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	staticNodesHelper.SetPositionAllocator (allocator);
	staticNodesHelper.Install(staticNodes);

	Ptr<ListPositionAllocator> mcPosition = CreateObject<ListPositionAllocator> ();
	mcPosition->Add (Vector(500, 500, ENB_HEIGHT));
	mcPosition->Add (Vector(2500, 2500, ENB_HEIGHT));
	mcPosition->Add (Vector(500, 2500, ENB_HEIGHT));
	mcPosition->Add (Vector(2500, 500, ENB_HEIGHT));

	MobilityHelper mcHelper;
	mcHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mcHelper.SetPositionAllocator(mcPosition);
	mcHelper.Install(mcNodes);
	BuildingsHelper::Install(mcNodes);

	Ptr<ListPositionAllocator> scPosition = CreateObject<ListPositionAllocator> ();
	scPosition->Add (Vector(1500, 1150, ENB_HEIGHT));
	scPosition->Add (Vector(1500, 1850, ENB_HEIGHT));
	scPosition->Add (Vector(1150, 1500, ENB_HEIGHT));
	scPosition->Add (Vector(1850, 1500, ENB_HEIGHT));
	scPosition->Add (Vector(1500, 0, ENB_HEIGHT));
	scPosition->Add (Vector(1500, 3000, ENB_HEIGHT));
	scPosition->Add (Vector(0, 1500, ENB_HEIGHT));
	scPosition->Add (Vector(3000, 1500, ENB_HEIGHT));

	MobilityHelper scHelper;
	scHelper.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	scHelper.SetPositionAllocator(scPosition);
	scHelper.Install(scNodes);
	BuildingsHelper::Install(scNodes);

	MobilityHelper UAVHelper;
	auto uavPosition = generatePositionAllocator(numUAVs, 3000, UAV_HEIGHT, "list");
	if (algorithm == "random") {
		UAVHelper.SetMobilityModel("ns3::RandomWalk2dMobilityModel",
								"Mode", StringValue ("Time"),
								"Time", StringValue ("5s"),
								"Speed", StringValue ("ns3::ConstantRandomVariable[Constant=" +
													to_string(uav_speed) + "]"),
								"Bounds", StringValue ("0|3000|0|3000"));
	} else {
		UAVHelper.SetMobilityModel("ns3::WaypointMobilityModel",
								   "InitialPositionIsWaypoint", BooleanValue (true));
	}
	UAVHelper.SetPositionAllocator (uavPosition);
	UAVHelper.Install (uavNodes);
	BuildingsHelper::Install(uavNodes);

	std::ostringstream oss;
	for (auto iter = uavNodes.Begin(); iter < uavNodes.End(); ++iter) {
		uint32_t id = (*iter)->GetId();
		oss << "/NodeList/" << id << "/$ns3::MobilityModel/CourseChange";
		Config::Connect(oss.str(), MakeCallback(&CourseChange));
		oss.str("");
	}

	Ns2MobilityHelper UEHelper = Ns2MobilityHelper(ns3_dir + mobil_trace);
	UEHelper.Install(UEs.Begin(), UEs.End());
	BuildingsHelper::Install(UEs);
}

void installEnergy(NodeContainer uavs){
	/*
	* Create and install energy source on the nodes using helper.
	*/
	// source helper
	BasicEnergySourceHelper basicSourceHelper;
	basicSourceHelper.Set ("BasicEnergySourceInitialEnergyJ", DoubleValue (350e3));
	// set update interval
	basicSourceHelper.Set ("PeriodicEnergyUpdateInterval",
						 TimeValue (Seconds (1.5)));
	// install source
	basicSourceHelper.Install (uavs);
}

void NetworkStatsMonitor(FlowMonitorHelper* fmhelper, Ptr<FlowMonitor> flowMon)
{
    flowMon->CheckForLostPackets();
    uint32_t LostPacketsum = 0;
	float PDR, PLR, Delay, Jitter, Throughput;
    std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats();
    Ptr<Ipv4FlowClassifier> classing = DynamicCast<Ipv4FlowClassifier>(fmhelper->GetClassifier());
	std::ofstream qos_file;
	qos_file.open("qos.txt", std::ofstream::out | std::ofstream::trunc);
	auto ue_network = Ipv4Address("7.0.0.0");
	auto ue_network_mask = Ipv4Mask("255.0.0.0");

    for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin(); stats != flowStats.end(); ++stats) {
        Ipv4FlowClassifier::FiveTuple fiveTuple = classing->FindFlow(stats->first);

		if(!ue_network_mask.IsMatch(ue_network, fiveTuple.destinationAddress))
			continue;

		PDR = (100 * stats->second.rxPackets) / (stats->second.txPackets);
        LostPacketsum = (stats->second.txPackets) - (stats->second.rxPackets);
		PLR = ((LostPacketsum * 100) / stats->second.txPackets);
		Delay = (stats->second.delaySum.GetSeconds()) / (stats->second.rxPackets);
		Jitter = (stats->second.jitterSum.GetSeconds()) / (stats->second.rxPackets);
		Throughput = stats->second.rxBytes * 8.0 / (stats->second.timeLastRxPacket.GetSeconds() - stats->second.timeFirstTxPacket.GetSeconds()) / 1024 / 1024;

        std::cout << "Flow ID			: " << stats->first << " ; " << fiveTuple.sourceAddress << " -----> " << fiveTuple.destinationAddress << std::endl;
        std::cout << "Tx Packets = " << stats->second.txPackets << std::endl;
        std::cout << "Rx Packets = " << stats->second.rxPackets << std::endl;
        std::cout << "Lost Packets = " << (stats->second.txPackets) - (stats->second.rxPackets) << std::endl;
        std::cout << "Packets Delivery Ratio (PDR) = " << PDR << "%" << std::endl;
        std::cout << "Packets Lost Ratio (PLR) = " << PLR << "%" << std::endl;
        std::cout << "Delay = " << Delay << " Seconds" << std::endl;
		std::cout << "Jitter = " << Jitter << " Seconds" << std::endl;
        std::cout << "Total Duration		: " << stats->second.timeLastRxPacket.GetSeconds() - stats->second.timeFirstTxPacket.GetSeconds() << " Seconds" << std::endl;
        std::cout << "Last Received Packet	: " << stats->second.timeLastRxPacket.GetSeconds() << " Seconds" << std::endl;
        std::cout << "Throughput: " << Throughput << " Mbps" << std::endl;
        std::cout << "---------------------------------------------------------------------------" << std::endl;
		uint16_t app_port = fiveTuple.destinationPort;
		Task t = port2Task[app_port];
		qos_file << fiveTuple.sourceAddress << " --> " << fiveTuple.destinationAddress << "," << PDR << "," << PLR << "," << Delay << "," << Jitter << "," << Throughput << "," << t.get_name() << "\n";
    }

	qos_file.close();
}

unsigned int id_from_context(std::string context)
{
	size_t start = 10;
	size_t end;
	string s_id;
	unsigned int id;

	end = context.find("/", start);
	s_id = context.substr(start, end - start);
	id = stoul(s_id);

	return id;
}

void CourseChange(std::string context, Ptr<const MobilityModel> model)
{
	if (model->GetVelocity().GetLength() > 0)
		return; //still moving

	unsigned int id = id_from_context (context);
	id = get_relative_id(id, NodeType::UAV);
	UAVStatus& status = uav_status.at(id);
	double movement_end = Simulator::Now().GetSeconds();
	double distance = (movement_end-status.movement_start) * uav_speed;

	status.mobility_log << id
		<< "," << status.movement_start
		<< "," << movement_end
		<< "," << distance
		<< std::endl;
}

void move_uav(Ptr<Node> uav, Vector destination, double n_vel) {
	// get mobility model for uav
    Ptr<WaypointMobilityModel> mob = uav->GetObject<WaypointMobilityModel>();
    Vector m_position = mob->GetPosition();
    double distance;

	distance = CalculateDistance(destination, m_position);
	// 1 meter of accuracy is acceptable
	if(distance <= 1)
		return;

	unsigned int id = get_relative_id(uav->GetId(), NodeType::UAV);
	UAVStatus& status = uav_status.at(id);
	double currentTime = Simulator::Now().GetSeconds();

	if(mob->GetVelocity().GetLength() == 0)
		status.movement_start = currentTime;

	double nWaypointTime;
	NS_LOG_DEBUG("moving uav with id: " << id << " from " << m_position << " to " << destination << " time: " << currentTime);

	mob->EndMobility();
	mob->AddWaypoint(Waypoint(Simulator::Now(), m_position));

	nWaypointTime = distance/n_vel + currentTime;
	mob->AddWaypoint(Waypoint(Seconds(nWaypointTime), destination));
}

void write_energy_trace(EnergyType eType, unsigned int id, double energy_spent, double remaining_energy)
{
	double currentTime = Simulator::Now().GetSeconds();
	UAVEnergyTrace& eTrace = uav_energy_trace;
	std::ostringstream message;

	message << currentTime
			<< " " << id
			<< " " << energy_spent
			<< " " << remaining_energy
			<< "\n";

	switch(eType)
	{
		case EnergyType::mobility:
			eTrace.mobility << message.str();
			break;

		case EnergyType::comms:
			eTrace.comms << message.str();
			break;

		case EnergyType::compute:
			eTrace.compute << message.str();
			break;
	}
}

Ptr<BasicEnergySource> get_energy_source(Ptr<Node> uav){
	Ptr<EnergySourceContainer> energy_container;
	Ptr<BasicEnergySource> source;

	energy_container = uav->GetObject<EnergySourceContainer> ();
	source = DynamicCast<BasicEnergySource> (energy_container->Get(0));

	return source;
}

void write_uav_status(unsigned int id,Vector pos, double remaining_energy)
{
	double currentTime = Simulator::Now().GetSeconds();
	std::ostringstream message;

	message << currentTime
			<< " " << id
			<< " " << pos.x
			<< " " << pos.y
			<< " " << remaining_energy;

	UAVStatus::status << message.str() << endl;
}

static double calc_process_time(double task_size)
{
	double Li = task_size;
	return beta_i0 * Li * Ci / fiUAV;
}

static double calc_process_energy(double task_process_time)
{
	double k = 1.0e-28;
	double tiUAV = task_process_time;

	return k * pow(fiUAV, 3) * tiUAV;
}

void update_mobility_energy(NodeContainer uavs)
{
	Ptr<Node> uav;
	unsigned int id;
	Vector pos;
	Ptr<BasicEnergySource> source;
	double energy_spent, remaining_energy;

	for (auto iter = uavs.Begin(); iter != uavs.End(); iter++)
	{
		uav = *iter;

		id = get_relative_id(uav->GetId(), NodeType::UAV);
		pos = uav->GetObject<MobilityModel>()->GetPosition ();

		source = get_energy_source(uav);

		//Ordem de entrada dos parametros: posição X, posição Y, posição Z, tempo de atualização, velocidade
		energy_spent = source->UpdateEnergyMobSource(pos.x,pos.y,pos.z, MOBILITY_ENERGY_INTERVAL, uav_speed);
		remaining_energy = source->GetRemainingEnergy();

		write_energy_trace(EnergyType::mobility, id, energy_spent, remaining_energy);
		write_uav_status(id, pos, remaining_energy);
	}

	Simulator::Schedule(Seconds(MOBILITY_ENERGY_INTERVAL), &update_mobility_energy, uavs);
}

void update_comms_energy(NodeContainer uavs){
	Ptr<Node> uav;
	unsigned int id;
	Ptr<BasicEnergySource> source;
	double remaining_energy;

	for (auto iter = uavs.Begin(); iter != uavs.End(); iter++)
	{
		uav = *iter;
		id = uav->GetId();
		source = get_energy_source(uav);
		source->ProcessEnergy(COMMS_ENERGY_COST);

		remaining_energy = source->GetRemainingEnergy();
		write_energy_trace(EnergyType::comms, id, COMMS_ENERGY_COST, remaining_energy);
	}

	Simulator::Schedule(Seconds(COMMS_ENERGY_INTERVAL), &update_comms_energy, uavs);
}

void update_compute_energy(Ptr<Node> uav, double energy_spent){
	unsigned int id;
	Ptr<BasicEnergySource> source;
	double remaining_energy;

	id = uav->GetId();
	source = get_energy_source(uav);
	source->ProcessEnergy(energy_spent);

	remaining_energy = source->GetRemainingEnergy();
	write_energy_trace(EnergyType::compute, id, energy_spent, remaining_energy);
}

std::string exec(std::string cmd)
{
  std::array<char, 128> buffer;
  std::string result;
  std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
  if (!pipe)
    throw std::runtime_error("popen() failed!");
  while (!feof(pipe.get()))
  {
    if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
      result += buffer.data();
  }
  return result;
}

Ptr<ListPositionAllocator>
generatePositionAllocator(int number_of_nodes = 300, int area = 1000,
						  float height = 45, std::string allocation = "random")
{

  Ptr<ListPositionAllocator> HpnPosition =
      CreateObject<ListPositionAllocator>();
  Ptr<UniformRandomVariable> distribution = CreateObject<UniformRandomVariable> ();
  distribution->SetAttribute ("Min", DoubleValue (0));
  distribution->SetAttribute ("Max", DoubleValue (area));

  if (allocation == "koln")
  {
    std::ifstream cellList("traces/cellList_koln");
    double a, b, c;
    while (cellList >> a >> b >> c)
    {
      NS_LOG_DEBUG("adding cell to position " << b * distance_multiplier << " "
                                     << c * distance_multiplier);
      HpnPosition->Add(
          Vector3D(b * distance_multiplier, c * distance_multiplier, height));
    }
  }

  else if (allocation == "random")
  {
    for (int i = 0; i < number_of_nodes; i++)
    {
      HpnPosition->Add(
          Vector3D(distribution->GetInteger(), distribution->GetInteger(), height));
    }
  }

  else if (allocation == "list")
  {
	HpnPosition->Add (Vector(1500, 1148, height));
	HpnPosition->Add (Vector(1500, 1852, height));
	HpnPosition->Add (Vector(1148, 1500, height));
	HpnPosition->Add (Vector(1852, 1500, height));

	HpnPosition->Add (Vector(505, 505, height));
	HpnPosition->Add (Vector(2505, 2505, height));
	HpnPosition->Add (Vector(505, 2505, height));
	HpnPosition->Add (Vector(2505, 505, height));

	HpnPosition->Add (Vector(505, 495, height));
	HpnPosition->Add (Vector(2505, 2495, height));
	HpnPosition->Add (Vector(505, 2495, height));
	HpnPosition->Add (Vector(2505, 495, height));
  }

  else
  {
    NS_FATAL_ERROR("invalid cell allocation policy.");
  }
  return HpnPosition;
}

std::vector<std::pair<int, int>> create_hot_spots()
{
  std::vector<std::pair<int, int>> centers;
  std::uniform_int_distribution<int> distribution(0, 2000);

  for (uint32_t i = 0; i < number_of_hot_spots; i++)
  {
    std::pair<int, int> center;
    center.first = distribution(generator);
    center.second = distribution(generator);
    NS_LOG_DEBUG("Adding centroid to " << center.first << " " << center.second);
    centers.push_back(center);
  }

  return centers;
}

std::map<std::pair<int, int>, double> populate_requests_trace()
{
  int multiplier = 1024;
  std::map<std::pair<int, int>, double> requests; //

  // at 978 user 99 requests 1043.98848 bytes
  std::ifstream trace_file(ns3_dir + requests_trace);
  std::string str;

  while (std::getline(trace_file, str))
  {
    vector<std::string> split_string;
    boost::split(split_string, str, boost::is_any_of(" "));

    // pegar triple time [1], user [], valor da request
    if (verbose)
    {
      NS_LOG_DEBUG(split_string[1] << " " << split_string[3] << " " << split_string[5]
                          << " ");
    }
    int user = stoi(split_string[3]);
    int time = stoi(split_string[1]);
    double request_value = stod(split_string[5]) * multiplier; // save in bytes
    requests[{time, user}] = request_value;
  }
  trace_file.close();
  return requests;
}

// populate pairing from nodeid to imsi
int populate_path_imsi(std::string path, int imsi)
{
  int nodeid = -1;

  std::vector<std::string> split_path;
  boost::split(split_path, path, boost::is_any_of("/"));
  nodeid = stoi(split_path[2]);

  if (imsi != -1)
    path_imsi[nodeid] = imsi;

  return nodeid;
}

int get_nodeid_from_path(std::string path)
{
  int nodeid;

  std::vector<std::string> split_path;
  boost::split(split_path, path, boost::is_any_of("/"));
  nodeid = stoi(split_path[2]);

  // if key is present, return nodeid which is 1 less than imsi
  if (path_imsi.find(nodeid) != path_imsi.end())
    return path_imsi[nodeid] - 1;
  return -1;
}

int get_absolute_node_id_from_path(std::string path)
{
	int nodeid;

	std::vector<std::string> split_path;
	boost::split(split_path, path, boost::is_any_of("/"));
	nodeid = stoi(split_path[2]);

	return nodeid;
}

// getter methods
Vector get_node_position(Ptr<Node> node)
{
  Ptr<MobilityModel> mob = node->GetObject<MobilityModel>();
  return mob->GetPosition();
}

int getCellId(int user_id)
{
  for (uint32_t i = 0; i < numCells; i++)
  {
    if (cellUe[i][user_id])
    {
      return i;
    }
  }
  return -1;
}

// get node imsi from cellId and rnti
int get_imsi(int cellId, int rnti)
{
  return rnti_cells[cellId][rnti] == 0 ? -1 : rnti_cells[cellId][rnti];
}

int get_cell(int user_id)
{
  for (uint32_t i = 0; i < numCells; i++)
  {
    if (cellUe[i][user_id])
    {
      return i;
    }
  }
  return -1;
}

int get_cell_from_imsi(int imsi)
{
  int servingCell = 0;
  for (uint32_t i = 0; i < numCells; i++)
  {
    if (cellUe[i][imsi - 1] != 0)
    {
      servingCell = i;
    }
  }
  return servingCell;
}

bool is_drone(uint32_t node_id) { return node_id >= numMacroCells+numSmallCells; }

int getNodeId(Ptr<Node> node, string type = "server")
{
  // seleced the desired node container
  NodeContainer tmpNodesContainer;
  if (type == "server" || type == "enb")
    tmpNodesContainer = BSNodes;
  else if (type == "ue")
    tmpNodesContainer = ueNodes;

  // find th enode id
  for (uint32_t i = 0; i < tmpNodesContainer.GetN(); ++i)
  {
    if (node == tmpNodesContainer.Get(i))
    {
      // NS_LOG_UNCOND("node " << node << " is " << tmpNodesContainer.Get(i) <<
      // " ?");
      return i;
    }
  }

  return -1;
}

static int get_relative_id(int absolute_id, NodeType type)
{
	int id_base;
	int relative_id;
	switch(type) {
		case NodeType::STATIC:
			id_base = 0;
			break;

		case NodeType::GBS:
			id_base = GBSNodes.Get(0)->GetId();
			NS_ASSERT_MSG(absolute_id >= id_base, "Absolute id too small. Node type: GBS");
			break;

		case NodeType::UAV:
			id_base = uavNodes.Get(0)->GetId();
			NS_ASSERT_MSG(absolute_id >= id_base, "Absolute id too small. Node type: UAV");
			break;

		case NodeType::UE:
			id_base = ueNodes.Get(0)->GetId();
			NS_ASSERT_MSG(absolute_id >= id_base, "Absolute id too small. Node type: UE");
			break;
	}
	relative_id = absolute_id - id_base;
	return relative_id;
}

int cell_to_id(CellId cell)
{
	return (cell.getValue() - 1)/num_ccs;
}

int cell_to_relative_id(CellId cell, NodeType type)
{
	int id = cell_to_id(cell);

	if(type == NodeType::UAV)
		id -= (numMacroCells + numSmallCells);
	else if(type != NodeType::GBS)
		NS_FATAL_ERROR("Invalid NodeType!");

	return id;
}

int relative_id_to_cell(int id, NodeType type)
{
	int cell;

	if(type == NodeType::GBS)
		cell = id;
	else if(type == NodeType::UAV)
		cell = numMacroCells + numSmallCells + id;
	else
		NS_FATAL_ERROR("Invalid NodeType!");

	return cell * num_ccs + 1;
}

bool is_primary(int cell)
{
	return (cell-1)%num_ccs == 0;
}

int getEdge(int nodeId)
{
  int edgeId = -1;
  for (uint32_t i = 0; i < numEdgeServers; ++i)
    if (edgeUe[i][nodeId])
    {
      edgeId = i;
    }
  return edgeId;
}

int get_closest_center_index(Ptr<Node> node,
                             std::vector<std::pair<int, int>> centers)
{
  Vector m_position = get_node_position(node);
  double dist = INT_MAX;
  int closest = -1;

  if (centers.size() == 0)
    return closest;

  for (uint32_t i = 0; i < number_of_hot_spots; i++)
  {
    if (dist > CalculateDistance(m_position, Vector3D(centers[i].first,
                                                      centers[i].second, 1)))
    {
      closest = i;
    }
  }
  return closest;
}

// this is not workiiiing
int get_user_id_from_ipv4(Ipv4Address ip)
{

  for (uint32_t i = 0; i < numUEs; i++)
  {
    if (user_ip[i] == ip)
    {
      return i;
    }
  }
  return -1;
}

void HandoverPrediction(int nodeId, int timeWindow)
{
    std::string mobilityTrace = ns3_dir + mobil_trace;

    // means no connection has been found
    // happens if it's called too early in the simulation
    if (getCellId(nodeId) == -1)
        return;

    // receive a nodeId, and a time window, and return if a handover is going to happen in this time window.
    std::ifstream mobilityFile(mobilityTrace);
    NS_ASSERT_MSG(mobilityFile.is_open(), "Error opening prediction file.");

    string nodeColumn;
    string fileLines;

    // coordinate variables
    double node_x, node_y, node_z, node_position_time;
    double shortestDistance = numeric_limits<int>::max();
    int closestCell = numeric_limits<int>::max();

    // tmp veriables to read file
    // node_position_time = time of the position
    string aux1, aux2, aux4, aux5;
    string cell_id;

    while (getline(mobilityFile, fileLines))
    {
        if (fileLines.find("setdest") != string::npos)
        {

            std::stringstream ss(fileLines);
            // cout << ss.str();
            ss >> aux1 >> aux2 >> node_position_time >> aux4 >> aux5 >> node_x >> node_y >> node_z;

            nodeColumn = "\"$node_(" + to_string(nodeId) + ")";
            // cout << "nodeColumn" << nodeColumn << "\n";
            // cout << "aux: " << aux4 << "\n";
            // cin.get();

            // for (int time_offset = 0; time_offset < timeWindow; time_offset++)
            if (aux4 == nodeColumn && Simulator::Now().GetSeconds() + timeWindow == round(node_position_time))
            {
                Vector uePos = Vector(node_x, node_y, node_z);

                // double distanceServingCell = CalculateDistance(uePos, enbNodes.Get(getCellId(nodeId))->GetObject<MobilityModel>()->GetPosition ());

                // calculate distance from node to each enb
                for (unsigned int i = 0; i < numBSs; ++i)
                {
                    // get Ith enb  position
                    Vector enbPos = BSNodes.Get(i)->GetObject<MobilityModel>()->GetPosition();
                    // get distance
                    double distanceUeEnb = CalculateDistance(uePos, enbPos);

                    // get closest enb
                    if (distanceUeEnb < shortestDistance)
                    {
                        closestCell = i;
                        shortestDistance = distanceUeEnb;
                    }
                }

                // if closest enb != current, predict handover
                if (closestCell != getCellId(nodeId))
                {
                    std::cout << "Handover to happen at " << node_position_time << endl;
                    std::cout << "Node " << nodeId << " from cell " << getCellId(nodeId) << " to cell " << closestCell << endl;
                    handoverPredictions[nodeId][0] = node_position_time;
                    handoverPredictions[nodeId][1] = getCellId(nodeId);
                    handoverPredictions[nodeId][2] = closestCell;
                }
            }
        }
    }

    mobilityFile.close();
}

// ================ EVENT LISTENERS ====================
void ReportUeMeasurementsCallback(std::string path, uint16_t rnti,
                                  uint16_t cellId, double rsrp, double rsrq,
                                  bool servingCell, uint8_t componentCarrierId)

{
  int imsi = get_imsi(cellId, rnti);
  int absolute_node_id = get_absolute_node_id_from_path(path);
	int node_id = get_relative_id(absolute_node_id, NodeType::UE);

	populate_path_imsi(path, imsi);

  if (verbose)
  {
    NS_LOG_DEBUG("Simulation time: " << Simulator::Now().GetSeconds());
    NS_LOG_DEBUG(path);
    NS_LOG_DEBUG("rnti " << rnti);
    NS_LOG_DEBUG("cellid " << cellId);
    NS_LOG_DEBUG("rsrp " << rsrp);
    NS_LOG_DEBUG("rsrq " << rsrq);
    NS_LOG_DEBUG("imsi " << imsi);
    NS_LOG_DEBUG("path imsi " << path_imsi[node_id]);
    NS_LOG_DEBUG("serving cell " << servingCell);
    NS_LOG_DEBUG("cc id " << (int)componentCarrierId);
    NS_LOG_DEBUG("\n");
  }
  // store all received signals here, must define a signal threhold to ignore
  // cells that are no longer reachable
	neighbors[cellId - 1][node_id] = rsrp;
  // call handover manager upon receiving new measurements
  handoverManager(path);
}

void RecvMeasurementReportCallback(std::string path, uint64_t imsi,
                                   uint16_t cellId, uint16_t rnti,
                                   LteRrcSap::MeasurementReport meas)
{
  if (verbose)
  {
    NS_LOG_DEBUG("Simulation time: " << Simulator::Now().GetSeconds());
    NS_LOG_DEBUG(path);
    NS_LOG_DEBUG(imsi);
    NS_LOG_DEBUG(cellId);
    NS_LOG_DEBUG(rnti);
    NS_LOG_DEBUG((int)meas.measResults.measId);
    NS_LOG_DEBUG("\n");
  }
}

void add_ue_to_cell(CellId cell_id, int ue_id){
	if(is_drone(cell_to_id(cell_id))) {
		int uav_id = cell_to_relative_id(cell_id, NodeType::UAV);
		uav_connected_ues[uav_id].insert(ue_id);
	} else {
		int gbs_id = cell_to_relative_id(cell_id, NodeType::GBS);
		gbs_connected_ues[gbs_id].insert(ue_id);
	}
}

void remove_ue_from_cell(CellId cell_id, int ue_id){
	if(is_drone(cell_to_id(cell_id))) {
		int uav_id = cell_to_relative_id(cell_id, NodeType::UAV);
		uav_connected_ues[uav_id].erase(ue_id);
	} else {
		int gbs_id = cell_to_relative_id(cell_id, NodeType::GBS);
		gbs_connected_ues[gbs_id].erase(ue_id);
	}
}

void NotifyConnectionEstablishedUe(string context,
                                   uint64_t imsi,
                                   uint16_t cellid,
                                   uint16_t rnti)
{
	int oldCell;
	int bs_id;

    NS_LOG_INFO(Simulator::Now().GetSeconds() << " " << context << " UE IMSI " << imsi << ": connected to CellId " << cellid << " with RNTI " << rnti << "\n");

    std::stringstream temp_cell_dir;
    std::stringstream ueId;
    temp_cell_dir << "./v2x_temp/" << cellid;
    ueId << temp_cell_dir.str() << "/" << rnti;
    if (mkdir(temp_cell_dir.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
    {
    }
    ofstream outfile(ueId.str().c_str());
    outfile << imsi << endl;
    outfile.close();

	oldCell = get_cell(imsi - 1);
	if(oldCell != -1) {
		cellUe[oldCell][imsi - 1] = 0;
		CellId oldCellId = CellId::fromZeroBasedId(oldCell);
		remove_ue_from_cell(oldCellId, imsi - 1);
	}

	CellId cellId = CellId::fromRaw(cellid);
	handover_pending[imsi - 1] = false;
	bs_id = cell_to_id(cellId);
	edgeUe[bs_id][imsi - 1] = 1;
    cellUe[cellid - 1][imsi - 1] = rnti;
	add_ue_to_cell(cellId, imsi - 1);
	rnti_cells[cellid][rnti] = imsi;
}

void NotifyHandoverStartUe(string context,
                           uint64_t imsi,
                           uint16_t cellid,
                           uint16_t rnti,
                           uint16_t targetCellId)
{
    std::cout << Simulator::Now().GetSeconds() << " " << context << " UE IMSI " << imsi << ": previously connected to CellId " << cellid << " with RNTI " << rnti << ", doing handover to CellId " << targetCellId << "\n";

    if (algorithm == "greedy")
    {
        // migration of handover start
        edgeUe[cellid - 1][imsi - 1] = 0;
        edgeUe[targetCellId - 1][imsi - 1] = 1;
    }

    ++handNumber;
}

void NotifyHandoverEndOkUe(string context,
                           uint64_t imsi,
                           uint16_t cellid,
                           uint16_t rnti)
{
    std::cout << Simulator::Now().GetSeconds() << " " << context << " UE IMSI " << imsi << ": successful handover to CellId " << cellid << " with RNTI " << rnti << "\n";

    std::stringstream target_cell_dir;
    std::stringstream newUeId;
    target_cell_dir << "./v2x_temp/" << cellid;
    newUeId << target_cell_dir.str() << "/" << rnti;
    if (mkdir(target_cell_dir.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
    {
    }
    ofstream outfile(newUeId.str().c_str());
    outfile << imsi << endl;
    outfile.close();

	int oldCell = get_cell(imsi - 1);
	CellId oldCellId = CellId::fromZeroBasedId(oldCell);
	NS_ASSERT_MSG(oldCell != -1, "cell to ue map corrupted!");
	cellUe[oldCell][imsi - 1] = 0;
	remove_ue_from_cell(oldCellId, imsi - 1);

	CellId cellId = CellId::fromRaw(cellid);
	handover_pending[imsi - 1] = false;
	cellUe[cellid - 1][imsi - 1] = rnti;
	add_ue_to_cell(cellId, imsi - 1);
	rnti_cells[cellid][rnti] = imsi;
}

void NotifyConnectionEstablishedEnb(string context,
                                    uint64_t imsi,
                                    uint16_t cellid,
                                    uint16_t rnti)
{
    std::cout << Simulator::Now().GetSeconds() << " " << context << " eNB CellId " << cellid << ": successful connection of UE with IMSI " << imsi << " RNTI " << rnti << "\n";
}

void NotifyHandoverStartEnb(string context,
                            uint64_t imsi,
                            uint16_t cellid,
                            uint16_t rnti,
                            uint16_t targetCellId)
{
    std::cout << Simulator::Now().GetSeconds() << " " << context << " eNB CellId " << cellid << ": start handover of UE with IMSI " << imsi << " RNTI " << rnti << " to CellId " << targetCellId << "\n";
}

void NotifyHandoverEndOkEnb(string context,
                            uint64_t imsi,
                            uint16_t cellid,
                            uint16_t rnti)
{
    std::cout << Simulator::Now().GetSeconds() << " " << context << " eNB CellId " << cellid << ": completed handover of UE with IMSI " << imsi << " RNTI " << rnti << "\n";
}

void PhySyncDetectionCallback(std::string context, uint64_t imsi, uint16_t rnti,
                              uint16_t cellId, std::string type,
                              uint8_t count)
{
  NS_LOG_DEBUG("PhySyncDetectionCallback imsi " << imsi << " cellid " << cellId
                                       << " rnti " << rnti);
}

void RadioLinkFailureCallback(std::string context, uint64_t imsi,
                              uint16_t cellId, uint16_t rnti)
{
  NS_LOG_DEBUG("RadioLinkFailur eCallback " << imsi << " cellid " << cellId << " rnti "
                                   << rnti);
}

/* ======================= TRAFFIC GENERATORS ===============*/

void requestApplication(Ptr<Node> ueNode, Ptr<Node> targetServer,
                        uint16_t applicationPort, Task t)
{
  // use this snippet here
  DataRateValue dataRateValue;
  dataRateValue = DataRate(t.size_bits() / 10);
  uint64_t bitRate = dataRateValue.Get().GetBitRate();
  uint32_t packetSize = 1024; // bytes
  double interPacketInterval = static_cast<double>(packetSize * 8) / bitRate;
  Time udpInterval = Seconds(interPacketInterval);

  Ptr<Ipv4> remoteIpv4 = ueNode->GetObject<Ipv4>();
  Ipv4Address remoteIpAddr =
      remoteIpv4->GetAddress(1, 0).GetLocal(); // Interface 0 is loopback

  double duration = t.size_bits() / bitRate;

  UdpServerHelper server(applicationPort);
  ApplicationContainer apps = server.Install(ueNode);
  apps.Start(Seconds(0));
  apps.Stop(Seconds(duration + 1));

  uint32_t MaxPacketSize = 1024;
  uint32_t numPackets = std::ceil(t.size_bytes() / MaxPacketSize);

  UdpClientHelper client(remoteIpAddr, applicationPort);
  client.SetAttribute("Interval", TimeValue(udpInterval));
  client.SetAttribute("PacketSize", UintegerValue(MaxPacketSize));
  client.SetAttribute("MaxPackets", UintegerValue(numPackets));
  ApplicationContainer appc = client.Install(targetServer);
  appc.Start(Seconds(0.1));
	Simulator::Schedule(Seconds(duration), &renew_task, ueNode, t);
}

void request_video(Ptr<Node> sender_node, Ptr<Node> receiver_node,
                   Ipv4Address targetServerAddress)
{
  static uint16_t m_port = 2000;
  static int request_id = 0;

  EvalvidServerHelper server(m_port);
  server.SetAttribute("SenderTraceFilename", StringValue("st_highway_cif.st"));

  server.SetAttribute("SenderDumpFilename",
                      StringValue("sd_" + std::to_string(request_id) + ".txt"));
  // server.SetAttribute("PacketPayload", UintegerValue(512));

  ApplicationContainer apps = server.Install(sender_node);
  apps.Start(Seconds(0));

  EvalvidClientHelper client(targetServerAddress, m_port);
  client.SetAttribute("ReceiverDumpFilename",
                      StringValue("rd_" + std::to_string(request_id) + ".txt"));
  apps = client.Install(receiver_node);
  apps.Start(Seconds(0));

  request_id++;
  m_port++;
}

void UDPApp(Ptr<Node> remoteHost, NodeContainer ueNodes)
{
  // Install and start applications on UEs and remote host

  ApplicationContainer serverApps;
  ApplicationContainer clientApps;
  Time interPacketInterval = MilliSeconds(50);
  uint16_t dlPort = 1100;
  uint16_t ulPort = 2000;
  int startTime = 2;
  Ptr<Ipv4> remoteIpv4 = remoteHost->GetObject<Ipv4>();
  Ipv4Address remoteIpAddr =
      remoteIpv4->GetAddress(1, 0).GetLocal(); // Interface 0 is loopback

  for (uint32_t u = 0; u < ueNodes.GetN(); ++u)
  {
    Ptr<Node> ue = ueNodes.Get(u);
    Ptr<Ipv4> ueIpv4 = ue->GetObject<Ipv4>();
    Ipv4Address ueIpAddr = ueIpv4->GetAddress(1, 0).GetLocal();
    ulPort++;

    if (!disableDl)
    {
      PacketSinkHelper dlPacketSinkHelper(
          "ns3::UdpSocketFactory",
          InetSocketAddress(Ipv4Address::GetAny(), dlPort));
      serverApps.Add(dlPacketSinkHelper.Install(ue));

      UdpClientHelper dlClient(ueIpAddr, dlPort);
      dlClient.SetAttribute("Interval", TimeValue(interPacketInterval));
      dlClient.SetAttribute("MaxPackets", UintegerValue(1000000));
      dlClient.SetAttribute("PacketSize", UintegerValue(1024));
      clientApps.Add(dlClient.Install(remoteHost));
    }

    if (!disableUl)
    {
      ++ulPort;
      PacketSinkHelper ulPacketSinkHelper(
          "ns3::UdpSocketFactory",
          InetSocketAddress(Ipv4Address::GetAny(), ulPort));
      serverApps.Add(ulPacketSinkHelper.Install(remoteHost));

      UdpClientHelper ulClient(remoteIpAddr, ulPort);
      ulClient.SetAttribute("Interval", TimeValue(interPacketInterval));
      ulClient.SetAttribute("MaxPackets", UintegerValue(1000000));
      ulClient.SetAttribute("PacketSize", UintegerValue(1024));
      clientApps.Add(ulClient.Install(ue));
    }
  }

  serverApps.Start(Seconds(1));
  clientApps.Start(Seconds(startTime));
}

void ThroughputMonitor(FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon)
{
  // count lost packets
  flowMon->CheckForLostPackets();

  uint32_t LostPacketsum = 0;
  float PDR, PLR, Delay, Jitter, Throughput;
  auto flowStats = flowMon->GetFlowStats();
  auto ue_network = Ipv4Address("7.0.0.0");
  auto ue_network_mask = Ipv4Mask("255.0.0.0");

  Ptr<Ipv4FlowClassifier> classing =
      DynamicCast<Ipv4FlowClassifier>(fmhelper->GetClassifier());
  std::ofstream qos_file;

  for (auto stats : flowStats)
  {
    // disconsider old flows by checking if last packet was sent or received
    // before this round management interval
    if (Simulator::Now() - stats.second.timeLastTxPacket > management_interval &&
		Simulator::Now() - stats.second.timeLastRxPacket > management_interval)
    {
      continue;
    }

    // find flow characteristics
    Ipv4FlowClassifier::FiveTuple fiveTuple = classing->FindFlow(stats.first);

	if(!ue_network_mask.IsMatch(ue_network, fiveTuple.destinationAddress))
		continue;

    PDR = (double)(100 * stats.second.rxPackets) / (stats.second.txPackets);
    LostPacketsum = (double)(stats.second.txPackets) - (stats.second.rxPackets);
    PLR = (double)(LostPacketsum * 100) / stats.second.txPackets;
    Delay = (stats.second.delaySum.GetSeconds()) / (stats.second.rxPackets);
	Jitter = (stats.second.jitterSum.GetSeconds()) / (stats.second.rxPackets);
    Throughput = stats.second.rxBytes * 8.0 /
                 (stats.second.timeLastRxPacket.GetSeconds() -
                  stats.second.timeFirstTxPacket.GetSeconds()) /
                 1024 / 1024;

    std::cout << "Flow ID     : " << stats.first << " ; "
              << fiveTuple.sourceAddress << " -----> "
              << fiveTuple.destinationAddress << std::endl;
    std::cout << "Tx Packets = " << stats.second.txPackets << std::endl;
    std::cout << "Rx Packets = " << stats.second.rxPackets << std::endl;
    std::cout << "Lost Packets = "
              << (stats.second.txPackets) - (stats.second.rxPackets)
              << std::endl;
    std::cout << "Packets Delivery Ratio (PDR) = " << PDR << "%" << std::endl;
    std::cout << "Packets Lost Ratio (PLR) = " << PLR << "%" << std::endl;
    std::cout << "Delay = " << Delay << " Seconds" << std::endl;
    std::cout << "Total Duration    : "
              << stats.second.timeLastRxPacket.GetSeconds() -
                     stats.second.timeFirstTxPacket.GetSeconds()
              << " Seconds" << std::endl;
    std::cout << "Last Received Packet  : "
              << stats.second.timeLastRxPacket.GetSeconds() << " Seconds"
              << std::endl;
    std::cout << "Throughput: " << Throughput << " Mbps" << std::endl;
    std::cout << "Throughput in bytes: " << Throughput * 12500000 << " Bps" << std::endl;
    // receiving node, used to catch user downlink traffic
    NS_LOG_DEBUG("target node = " << get_user_id_from_ipv4(
            fiveTuple.destinationAddress));
    std::cout << "-------------------------------------------------------------"
                 "--------------"
              << std::endl;

    // received id will be -1 in case it is not a mobile user
    int receiver_id = get_user_id_from_ipv4(fiveTuple.destinationAddress);
    if (receiver_id != -1)
    {
      user_delay[receiver_id] = Delay;
      user_jitter[receiver_id] = Jitter;
      user_throughput[receiver_id] = Throughput;
      user_pdr[receiver_id] = PDR;
      cell_throughput[getCellId(receiver_id)] += Throughput;
    }
  }

	std::ofstream qos_vs_time;
	qos_vs_time.open("qos-vs-time.txt", std::ofstream::out | std::ofstream::app);
  for(uint32_t ue=0; ue < numUEs; ++ue)
  {
	qos_vs_time << Simulator::Now().GetSeconds() << "," 
		<< ue << ","
		<< user_requests[ue].get_name() << ","
		<< user_delay[ue] << ","
		<< user_jitter[ue] << ","
		<< user_throughput[ue] << ","
		<< user_pdr[ue] << std::endl;
  }

  // schedule itself in 1sec
  Simulator::Schedule(management_interval, ThroughputMonitor, fmhelper,
                      flowMon);
}

static Vector get_centroid(std::set<int> users)
{
	int user_id;
	double xsum=0;
	double ysum=0;
	Ptr<Node> user;
	Ptr<MobilityModel> mob;

	for(auto iter=users.begin(); iter!=users.end(); iter++) {
		user_id = *iter;
		user = ueNodes.Get(user_id);
		mob = user->GetObject<MobilityModel>();
		xsum += mob->GetPosition().x;
		ysum += mob->GetPosition().y;
	}

	return Vector(xsum/users.size(), ysum/users.size(), 10);
}

void write_user_status(NodeContainer users) {
    std::ofstream user_status_file("user_status.txt");
    for (auto i = users.Begin(); i != users.End(); i++) {
		Ptr<Node> user = *i;
		uint32_t user_id = get_relative_id(user->GetId(), NodeType::UE);
        Ptr<MobilityModel> mob = user->GetObject<MobilityModel>();
        Vector pos = mob->GetPosition();
		Task t = user_requests[user_id];
        user_status_file << user_id << " " << pos.x << " " << pos.y << " " << pos.z << " " << t.get_priority() << "\n";
    }
    user_status_file.close();
}

std::list<Vector> get_cluster_centers() {
    std::ifstream infile("centers.txt");
    std::list<Vector> centers;
    double x, y, z;
    while (infile >> x >> y >> z) {
        Vector tmp = {x, y, z};
        centers.push_back(tmp);
    }
    infile.close();
    return centers;
}

Vector closest_center(Ptr<Node> drone, std::list<Vector> centers){
	double min = numeric_limits<double>::infinity();
	double distance;
	Vector drone_position = drone->GetObject<MobilityModel>()->GetPosition();
	Vector closest;
	for(auto&& center_position: centers) {
		distance = CalculateDistance(drone_position, center_position);
		if(distance < min){
			min = distance;
			closest = center_position;
		}
	}
	return closest;
}

std::list<Vector>::iterator find_center(Vector center, std::list<Vector>& centers){
	std::list<Vector>::iterator position;
	for(position = centers.begin(); position != centers.end(); ++position) {
		if(position->x == center.x &&
			position->y == center.y &&
			position->z == center.z){
			break;
		}
	}
	return position;
}

void power_on_drone(int droneId)
{
	Ptr<LteEnbNetDevice> dev = DynamicCast<LteEnbNetDevice> (enbDevs.Get(droneId + numMacroCells + numSmallCells));
	for(uint8_t i = 0; i<num_ccs; ++i) {
		Ptr<LteEnbPhy> phy = dev->GetPhy(i);
		phy->SetTxPower(UAVtxpower);
	}
}

void power_on_all_drones()
{
	for (uint16_t i = 0; i < numUAVs; ++i)
		power_on_drone(i);
}

void kmeans_manager(NodeContainer nodes, NodeContainer drones) {
    // save user positions to file
	write_user_status(nodes);
    // generate centers file
    std::string cmd = "python3 " + ns3_dir + "scratch/clustering.py -c " +
					std::to_string(drones.GetN()) + " kmeans";
	exec(cmd);

	std::vector<Cluster> results = parse_result_file();
	Ptr<Node> uav;
	for(Cluster res : results) {
		power_on_drone(res.uav_id);
		uav = uavNodes.Get(res.uav_id);
		move_uav(uav, res.new_position, uav_speed);
		for(int user_id : res.users) {
			guided_target_cell[user_id] = relative_id_to_cell(res.uav_id, NodeType::UAV);
		}
	}
}

void topsis_manager(NodeContainer ueNodes, NodeContainer uavNodes) {
	Ptr<Node> uav;
	std::vector<Cluster> results;

	results = topsis(ueNodes);
	for(Cluster r : results) {
		power_on_drone(r.uav_id);
		uav = uavNodes.Get(r.uav_id);
		move_uav(uav, r.new_position, uav_speed);
	}
}

std::vector<Cluster>
coop(NodeContainer users)
{
	write_user_status(users);

	std::string cmd = "python3 " + ns3_dir + "scratch/coop.py --cluster-radius " + to_string(cluster_radius)
		+ " --max-users " + to_string(UAV_MAX_USERS)
		+ " --enable-pso --enable-topsis";
	exec(cmd);

	std::vector<Cluster> results = parse_result_file();
	for(Cluster res : results) {
		for(int user_id : res.users) {
			guided_target_cell[user_id] = relative_id_to_cell(res.uav_id, NodeType::UAV);
		}
	}
	return results;
}

void coop_manager(std::set<int> poor_qos, NodeContainer uavNodes) {
	Ptr<Node> uav;
	std::set<int> selected;
	NodeContainer selected_nodes;

	//Add ues already served by uavs
	for(auto ue_list : uav_connected_ues)
		selected.insert(ue_list.begin(), ue_list.end());

	for(uint32_t i = 0; i < numMacroCells + numSmallCells; i++) {
		auto gbs_ue_list = gbs_connected_ues[i];
		auto gbs_num_ues = gbs_ue_list.size();
		Vector gbs_pos = get_node_position(GBSNodes.Get(i));
		std::vector<int> gbs_unserved(gbs_num_ues);
		unsigned int served;
		int num_ues_to_keep = 0;
		NodeContainer ues = ueNodes;

		auto new_end = std::set_intersection(
				gbs_ue_list.begin(), gbs_ue_list.end(),
				poor_qos.begin(), poor_qos.end(),
				gbs_unserved.begin());
		gbs_unserved.resize(new_end - gbs_unserved.begin());

		cout << "GBS " << i <<
			" total UEs: " << gbs_num_ues <<
			" unserved UEs: " << gbs_unserved.size() <<
			"\n";

		//check if gbs is overloaded
		if (gbs_num_ues <= GBS_MAX_USERS) {
			//the gbs isn't overloaded so the ues have poor
			//qos due to weak signal condition
			goto add_selected;
		}

		//the gbs is overloaded so the ues have poor
		//qos due to insufficient resources
		served = gbs_num_ues - gbs_unserved.size();
		if (served > GBS_MAX_USERS) {
			cout << "Served users > GBS_MAX_USERS.\n" <<
				"This shouldn't happen, maybe GBS_MAX_USERS is too low?\n";
			goto add_selected;
		}

		std::stable_sort(gbs_unserved.begin(), gbs_unserved.end(),
				[gbs_pos, ues](int a, int b){
					auto pos_a = get_node_position(ues.Get(a));
					auto pos_b = get_node_position(ues.Get(b));
					return CalculateDistance(pos_a, gbs_pos) < CalculateDistance(pos_b, gbs_pos);
				});
		num_ues_to_keep = GBS_MAX_USERS-served;

		add_selected:
		selected.insert(gbs_unserved.begin()+num_ues_to_keep,
						gbs_unserved.end());
	}

	for(auto ue_id : selected)
		selected_nodes.Add(ueNodes.Get(ue_id));

	std::vector<Cluster> results = coop(selected_nodes);
	for(Cluster r : results) {
		power_on_drone(r.uav_id);
		uav = uavNodes.Get(r.uav_id);
		move_uav(uav, r.new_position, uav_speed);
	}
}

void UAVManager()
{
	std::set<int> unserved;
	NodeContainer unserved_nodes;
	double delay, pdr;

	for(uint32_t i = 0; i < numUEs; i++) {
		delay = user_delay[i];
		pdr = user_pdr[i];
		if (delay > user_requests[i].get_delay() || pdr < 99.0) {
			unserved.insert(i);
			unserved_nodes.Add(ueNodes.Get(i));
		}
	}

	NS_LOG_INFO("unserved users: " << unserved.size());

	if(unserved.size() == 0)
		return;

	if(algorithm == "cooperative")
		coop_manager(unserved, uavNodes);
	else if(algorithm == "topsis")
		topsis_manager(unserved_nodes, uavNodes);
	else if(algorithm == "kmeans")
		kmeans_manager(unserved_nodes, uavNodes);

	Simulator::Schedule(management_interval, &UAVManager);
}

bool find_handover(Handover h)
{
  for (auto &handover_compare : handover_vector)
  {
    if (h == handover_compare)
    {
      // NS_LOG_DEBUG("Handover already requested, not repeating.");
      return true;
    }
  }
  return false;
}

std::vector<Cluster>
parse_result_file() {
	std::vector<Cluster> results;
    std::ifstream infile("centers.txt");
	std::string line;
	std::istringstream values;
    double x, y;

	if (!infile.is_open())
		return results;

    while (infile.good()) {
		struct Cluster r;
		std::vector<string> data;

		std::getline(infile, line);
		if(line.empty())
			continue;
		boost::split(data, line, boost::is_any_of(";"));

		r.uav_id = std::stoi(data[0]);

		values.str(data[1]);
		values >> x >> y;
		values.clear();
        Vector pos = {x, y, UAV_HEIGHT};
		r.new_position = pos;

		values.str(data[2]);
		std::list<int> users;
		for(int u; values >> u;)
			users.push_back(u);
		values.clear();

		r.users = users;
		results.push_back(r);
    }
    infile.close();
    return results;
}

std::vector<Cluster>
topsis(NodeContainer users)
{
	write_user_status(users);

	std::string cmd = "python3 " + ns3_dir + "scratch/decision.py --cluster-radius " + to_string(cluster_radius)
		+ " --enable-pso --enable-topsis";
	exec(cmd);

	std::vector<Cluster> results = parse_result_file();
	for(Cluster res : results) {
		for(int user_id : res.users) {
			guided_target_cell[user_id] = relative_id_to_cell(res.uav_id, NodeType::UAV);
		}
	}
	return results;
}

void schedule_handover(int id_user, int source_cell_id, int target_cell_id)
{
  // inter drones handovers may be tricky
  bool block_drones_handovers = false;
  bool random_time = false;
  int h_time = (int)Simulator::Now().GetSeconds();
  int max_handovers_per_second = 3;
  int id_source = cell_to_id(CellId::fromRaw(source_cell_id));
  int id_target = cell_to_id(CellId::fromRaw(target_cell_id));

  if(!is_primary(target_cell_id))
  {
	  NS_LOG_WARN("Ignoring handover to secondary cell");
	  return;
  }

  // reject handover if in this second more than the limit have been performed
  if (handovers_per_second.find(h_time) != handovers_per_second.end())
  {
    if (handovers_per_second[h_time] > max_handovers_per_second)
      return;
  }

  if (block_drones_handovers && is_drone(id_target) && is_drone(id_source))
  {
	  NS_LOG_WARN("Handover between drones forbidden");
      return;
  }

  // create handover identifier
  Handover handover(Simulator::Now().GetSeconds(), id_user, cellUe[id_source][id_user], source_cell_id,
                    target_cell_id);

  // if this handover has already been attempted, return.
  if (find_handover(handover))
  {
    NS_LOG_WARN("Handover already exists");
    return;
  }

  Ptr<LteUeNetDevice> ueLteDevice =
      ueDevs.Get(id_user)->GetObject<LteUeNetDevice>();
  Ptr<LteUeRrc> ueRrc = ueLteDevice->GetRrc();

  // NS_LOG_DEBUG("User device in state " << ueRrc->GetState());
  if (ueRrc->GetState() != LteUeRrc::CONNECTED_NORMALLY)
  {
    NS_LOG_WARN("Wrong LteUeRrc state!");
    return;
  }
  // NS_TEST_ASSERT_MSG_EQ (ueRrc->GetState (), LteUeRrc::CONNECTED_NORMALLY,
  // "Wrong LteUeRrc state!");

  Ptr<NetDevice> enbDev = enbDevs.Get(id_source);
  if (!enbDev)
  {
    NS_LOG_WARN(Simulator::Now().GetSeconds()
			<< " LTE eNB " << id_source << " device not found");
    return;
  }

  Ptr<LteEnbNetDevice> enbLteDevice = enbDev->GetObject<LteEnbNetDevice>();
  Ptr<LteEnbRrc> enbRrc = enbLteDevice->GetRrc();
  // uint16_t rnti = ueRrc->GetRnti();

  // NS_LOG_DEBUG("rnti " << rnti);
  // NS_LOG_DEBUG("user_id " << id_user);
  // NS_LOG_DEBUG("id_source " << id_source);
  // NS_LOG_DEBUG("id_target " << id_user);

  // this mf keeps returning error I dont know why
  // Ptr<UeManager> ueManager = enbRrc->GetUeManager(rnti);

  if (random_time)
  {
    // generate random time for the handover in order not to overcrowd random access
    std::uniform_real_distribution<> dis(0, 1.0);
    int handover_time = dis(generator);

    handover = Handover(handover_time, id_user, cellUe[id_source][id_user], source_cell_id, target_cell_id);
    NS_LOG_INFO(handover);
    handover_vector.push_back(handover);
    lteHelper->HandoverRequest(Seconds(handover_time), ueDevs.Get(id_user),
                               enbDevs.Get(id_source), target_cell_id);
  }
  else
  {
    NS_LOG_INFO(handover);
    handover_vector.push_back(handover);
    lteHelper->HandoverRequest(MilliSeconds(10), ueDevs.Get(id_user),
			enbDevs.Get(id_source), target_cell_id);
  }

  // control number of handovers performee
  if (handovers_per_second.find(h_time) == handovers_per_second.end())
    handovers_per_second[h_time] = 1;
  else
    handovers_per_second[h_time] += 1;
  // if handover is valid, add it to list of handovers

  handover_pending[id_user] = true;
}

void guided_handover(int signal_threshold, int ue_id, int source_cell_id)
{
	int target_cell_id;
	double target_cell_rsrp;
	double source_cell_rsrp;

	target_cell_id = guided_target_cell[ue_id];
	if(target_cell_id == -1)
		return;

	target_cell_rsrp = neighbors[target_cell_id - 1][ue_id];
	source_cell_rsrp = neighbors[source_cell_id - 1][ue_id];

	if (target_cell_rsrp > (signal_threshold + source_cell_rsrp) &&
		target_cell_id != source_cell_id)
	{
		NS_LOG_UNCOND("Handover scheduled -> source_cell: " << source_cell_id << " " << source_cell_rsrp
					<< " target_cell: " << target_cell_id << " " << target_cell_rsrp
					<< " ue: " << ue_id);
		schedule_handover(ue_id, source_cell_id, target_cell_id);
	}
}

void handoverManager(std::string path)
{

  int nodeid = get_nodeid_from_path(path);

  if (nodeid == -1)
    return;

  // user-wise strongest cell implementation
  int imsi = nodeid + 1;
  uint32_t servingCell = get_cell(nodeid);
  double rsrp = ZERO_SIGNAL_LEVEL;
  uint32_t bestNeighborCell = -1;
  int signal_threshold = 3;

  if (handover_pending[nodeid])
  {
	//NS_LOG_WARN("UE with IMSI " << imsi << " has a pending handover. Skipping.");
	return;
  }

  if ((int)servingCell == -1)
  {
    return;
  }

  //check if signal is too low to do a handover successfully
  if (neighbors[servingCell][nodeid] < CONNECTED_SIGNAL_LEVEL)
  {
	return;
  }

  if (handover_policy == "iuavbs")
  {
    // if user is not served
    if (unserved_users[nodeid])
    {
      // handover to closest drone
      for (uint32_t cell = 0; cell < numCells; cell++)
      {
        // define drone with highest signal as bset cell
        if (neighbors[cell][nodeid] > rsrp && cell != servingCell && is_drone(cell))
        {
          rsrp = neighbors[cell][nodeid];
          bestNeighborCell = cell;
        }
        if ((int)bestNeighborCell != -1 && bestNeighborCell != servingCell && Simulator::Now() > Seconds(1))
        {

          if (enableHandover)
          {
            schedule_handover(nodeid, servingCell+1, bestNeighborCell+1);
          }
        }
      }
    }
  }

  // this is taking a lot of time, why?
  else if (handover_policy == "classic")
  {
    for (uint32_t cell = 0; cell < numCells; cell++)
    {
      if (neighbors[cell][nodeid] > rsrp && cell != servingCell)
      {
        rsrp = neighbors[cell][nodeid];
        bestNeighborCell = cell;
      }
    }

    if (rsrp > (signal_threshold + neighbors[servingCell][imsi - 1]) &&
        bestNeighborCell != servingCell)
    {
      if (enableHandover)
      {
        schedule_handover(nodeid, servingCell+1, bestNeighborCell+1);
      }
    }
  }
  else if (handover_policy == "guided")
	  guided_handover(signal_threshold, nodeid, servingCell+1);
  else if (handover_policy == "none")
    return;
  else
  {
    NS_FATAL_ERROR("Handover policy type invalid.");
  }
}

// function that receives the user node container and checks if requirements are being met
double check_served(NodeContainer ueNodes)
{
    int counter_served = 0;

    for (uint32_t i = 0; i < ueNodes.GetN(); ++i)
    {
        for (uint32_t u = 0; u < numCells; ++u)
        {
            if (cellUe[u][i])
            {
                counter_served++;
                continue;
            }
        }
    }

    return (double)counter_served / ueNodes.GetN();
}

// function that receives the user node container and returns the percentage covered
double check_coverage(NodeContainer ueNodes)
{
    int counter_covered = 0;

    for (uint32_t i = 0; i < ueNodes.GetN(); ++i)
    {
        for (uint32_t u = 0; u < BSNodes.GetN(); ++u)
        {
            if (edgeUe[u][i])
            {
                counter_covered++;
                continue;
            }
        }
    }

    return (double)counter_covered / ueNodes.GetN();
}

double vec_average(std::vector<double> vec)
{

  int i = 0;
  double sum = 0;
  for (auto &v : vec)
  {
    i++;
    sum += v;
  }
  return sum / i;
}

void print_connections()
{
  for (uint32_t u = 0; u < numUEs; u++)
  {
    NS_LOG_DEBUG(getCellId(u));
  }

  NS_LOG_DEBUG("");

  for (uint32_t i = 0; i < numCells; i++)
  {
    for (uint32_t u = 0; u < numUEs; u++)
    {
      std::cout << cellUe[i][u] << " ";
    }
    NS_LOG_DEBUG("");
  }
}

void just_a_monitor()
{
	std::ofstream service_level("service-level.txt",
			std::ofstream::out | std::ofstream::app);

  // tmp variables
  double number_of_unserved = 0;
  double uav_usage_total = 0;
  double user_delay_total = 0;

  // print_connections();

	// loop over all users
	for (uint32_t i = 0; i < numUEs; i++)
	{
	  double delay = user_delay[i];
	  double pdr = user_pdr[i];
	  user_delay_total += user_delay[i];
	  bool served = true;

	  NS_LOG_DEBUG("User " << i << " delay " << delay);
	  NS_LOG_DEBUG("User " << i << " PDR " << pdr);
	  NS_LOG_DEBUG("User " << i << " request value " << user_requests[i]);

	  // user marked as unserved;
	  if (delay > user_requests[i].get_delay() || pdr < 99.0)
	  {
		number_of_unserved++;
		unserved_users[i] = true;
		served = false;
	  }

	  int cell = getCellId(i);
	  int bs_id = cell/num_ccs;
	  bool drone = is_drone(bs_id);
	  if (cell != -1)
	  {
		std::string cell_type = drone ? "UAV" : "GBS";
		NS_LOG_DEBUG("User " << i << " is in cell " << cell << " " << cell_type << ".");

		if (cell_type == "UAV")
		{
		  uav_usage_total++;
		}
	  }
	  else
	  {
		NS_LOG_DEBUG("User is not connected.");
	  }
	//service_level: "time,id,served,bs_id,bs_type"
	  service_level << Simulator::Now().GetSeconds() << ","
		  << i << ","
		  << served << ","
		  << bs_id - (numMacroCells + numSmallCells) * drone << ","
		  << (drone ? "uav" : "bs") << "\n";
	}

  // update metrics peek
  peek_service_level.push_back(1 - (number_of_unserved / numUEs));
  peek_uav_usage.push_back(uav_usage_total / numUEs);
  peek_user_delay.push_back(user_delay_total / numUEs);

  // print metrics peek
  NS_LOG_DEBUG("Simulation time: " << Simulator::Now().GetSeconds());
  NS_LOG_DEBUG("peek_service_level " << peek_service_level.back());
  NS_LOG_DEBUG("peek_uav_usage " << peek_uav_usage.back());
  NS_LOG_DEBUG("peek_user_delay " << peek_user_delay.back());

	service_level.close();
  Simulator::Schedule(management_interval, &just_a_monitor);
}

static void renew_task(Ptr<Node> user, Task task)
{
	static uint16_t applicationPort = 11000;
	double process_time;
	int user_id;
	Ptr<Node> uav;

	user_id = get_relative_id(user->GetId(), NodeType::UE);

	process_time = calc_process_time(task.size_mbits());
	//NS_LOG_INFO("User: " << user_id << " process time: " << process_time);

	int current_cell = get_cell(user_id);
	if (current_cell != -1) {
		CellId current_cell_id = CellId::fromZeroBasedId(current_cell);
		if ((algorithm == "topsis" || algorithm == "cooperative") &&
				is_drone(cell_to_id(current_cell_id))) {
			int current_uav = cell_to_relative_id(current_cell_id, NodeType::UAV);
			uav = uavNodes.Get(current_uav);
			update_compute_energy(uav, calc_process_energy(process_time));
			//request from uav edge
			requestApplication(user, uav, applicationPort, task);
			goto end;
		}
	}
	//request from cloud
	requestApplication(user, remoteHost, applicationPort, task);

end:
	port2Task[applicationPort] = task;
	applicationPort++;
}

static void assign_tasks(NodeContainer users)
{
	static std::vector<Task> task_types = {Task::VR, Task::AR, Task::VoD};
	Ptr<Node> user;
	int user_id;
	Task task;
	Ptr<UniformRandomVariable> index = CreateObject<UniformRandomVariable> ();

	for(auto iter=users.Begin(); iter!=users.End(); iter++) {
		user = *iter;
		user_id = get_relative_id(user->GetId(), NodeType::UE);
		//assign random task
		task = task_types[index->GetInteger(0, task_types.size()-1)];
		user_requests[user_id] = task;
		renew_task(user, task);
	}
}

int main(int argc, char* argv[])
{
	float simTime = 300.1;
	short remMode = 0; // [0]: REM disabled; [1]: generate REM at 1 second of simulation;
	//[2]: generate REM at the end of the simulation
	int32_t remRbId = -1;
	bool enableAnimation = false;

	LogComponentEnable ("uav-edge", LOG_LEVEL_INFO);

	CommandLine cmd;
	
	cmd.AddValue("algo","UAV management algorithm", algorithm);
	cmd.AddValue("numMCs", "how many macro cells are in the simulation", numMacroCells);
	cmd.AddValue("numSCs", "how many small cells are in the simulation", numSmallCells);
	cmd.AddValue("numUAVs", "how many UAVs are in the simulation", numUAVs);
	cmd.AddValue("numUEs", "how many UEs are in the simulation", numUEs);
	cmd.AddValue("mimo","MIMO mode", mimo_mode);
	cmd.AddValue("ca","enable carrier aggregation", useCa);
	cmd.AddValue("remMode","Radio environment map mode", remMode);
	cmd.AddValue ("remRbId", "Resource block Id, for which REM will be generated,"
                "default value is -1, what means REM will be averaged from all RBs", remRbId);
	cmd.AddValue("enableAnimation","Enable generation of the netAnim xml", enableAnimation);
	cmd.Parse(argc, argv);

	if(algorithm == "no-uavs")
		numUAVs = 0;

	if(algorithm == "topsis" ||
			algorithm == "cooperative" ||
			algorithm == "kmeans")
		handover_policy = "guided";

	ns3_dir = GetTopLevelSourceDir();

	num_ccs = useCa ? 3 : 1;
	// make sure there's hot spots even if the number of uavs is 0
	number_of_hot_spots = numUAVs == 0 ? 10 : numUAVs;
	numBSs = numMacroCells + numSmallCells + numUAVs;
	numCells = numBSs * num_ccs;
	numEdgeServers = numBSs;
	initialize_vectors();

	Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper>();

	Ptr<Node> pgw = config_LTE(lteHelper, epcHelper);

	NodeContainer remoteHostContainer;
	remoteHostContainer.Create(1);
	remoteHost = remoteHostContainer.Get(0);

	// create nodes in global containers
	mcNodes.Create(numMacroCells);
	scNodes.Create(numSmallCells);
	uavNodes.Create(numUAVs);
	ueNodes.Create(numUEs);

	// merge macro cells and small cells
	GBSNodes = NodeContainer(mcNodes, scNodes);
	// merge uav and bs nodes
	BSNodes = NodeContainer(GBSNodes, uavNodes);

	InternetStackHelper internet;
	internet.Install(remoteHostContainer);
    internet.Install(ueNodes);

	PointToPointHelper p2ph;
	p2ph.SetDeviceAttribute("DataRate", DataRateValue(DataRate("100Gb/s")));
	p2ph.SetDeviceAttribute("Mtu", UintegerValue(1400));
	p2ph.SetChannelAttribute("Delay", TimeValue(MilliSeconds(5)));
	NetDeviceContainer internetDevices = p2ph.Install(pgw, remoteHost);

	Ipv4AddressHelper ipv4h;
	ipv4h.SetBase("1.0.0.0", "255.0.0.0");
	Ipv4InterfaceContainer internetIpIfaces;
	internetIpIfaces = ipv4h.Assign(internetDevices);

	Ipv4StaticRoutingHelper ipv4RoutingHelper;

	Ipv4Address remoteHostAddr;
    remoteHostAddr = internetIpIfaces.GetAddress(1);
    Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting(remoteHost->GetObject<Ipv4>());
    remoteHostStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.0.0.0"), 1);
	for (uint32_t u = 0; u < ueNodes.GetN(); ++u) {
		Ptr<Node> ueNode = ueNodes.Get(u);
		Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting(ueNode->GetObject<Ipv4>());
		ueStaticRouting->SetDefaultRoute(epcHelper->GetUeDefaultGatewayAddress(), 1);
	}

	Ptr<Node> sgw = epcHelper->GetSgwNode();
	Ptr<Ipv4> sgwIpv4 = sgw->GetObject<Ipv4>();
	Ptr<Ipv4StaticRouting> sgwStaticRouting = ipv4RoutingHelper.GetStaticRouting(sgwIpv4);
	uint32_t sgw2pgwInterface = sgwIpv4->GetInterfaceForPrefix(Ipv4Address("14.0.0.0"),Ipv4Mask("255.255.255.0"));
	sgwStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.255.255.0"), sgw2pgwInterface);

	NodeContainer staticNodes;
	staticNodes.Add(epcHelper->GetSgwNode());
	staticNodes.Add(pgw);
	staticNodes.Add(remoteHost);
	install_mobility(staticNodes, mcNodes, scNodes, uavNodes, ueNodes);

	install_LTE(lteHelper, epcHelper, mcNodes, scNodes, uavNodes, ueNodes);

	for (uint32_t u = 0; u < mcNodes.GetN(); ++u) {
		Ptr<Node> BSNode = mcNodes.Get(u);
		Ptr<Ipv4> BSIpv4 = BSNode->GetObject<Ipv4>();
		Ptr<Ipv4StaticRouting> BSStaticRouting = ipv4RoutingHelper.GetStaticRouting(BSIpv4);
		BSStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.255.255.0"), 1);
	}

	for (uint32_t u = 0; u < uavNodes.GetN(); ++u) {
		Ptr<Node> uavNode = uavNodes.Get(u);
		Ptr<Ipv4> uavIpv4 = uavNode->GetObject<Ipv4>();
		Ptr<Ipv4StaticRouting> uavStaticRouting = ipv4RoutingHelper.GetStaticRouting(uavIpv4);
		uavStaticRouting->AddNetworkRouteTo(Ipv4Address("7.0.0.0"), Ipv4Mask("255.255.255.0"), 1);
	}

	installEnergy (uavNodes);

	// populate user ip map
	for (uint32_t i = 0; i < ueNodes.GetN(); i++)
	{
		Ptr<Ipv4> remoteIpv4 = ueNodes.Get(i)->GetObject<Ipv4>();
		Ipv4Address remoteIpAddr = remoteIpv4->GetAddress(1, 0).GetLocal();
		user_ip[i] = remoteIpAddr;
	}

	AnimationInterface* animator;
	if(enableAnimation) {
		animator = new AnimationInterface ("lte.xml");
		animator->SetMobilityPollInterval(Seconds(1));
		//animator->SetMaxPktsPerTraceFile(10000000);
		animator->SkipPacketTracing();
		for (uint32_t i = 0; i < mcNodes.GetN(); ++i) {
			animator->UpdateNodeDescription(mcNodes.Get(i), "MC " + std::to_string(i));
			animator->UpdateNodeColor(mcNodes.Get(i), 0, 200, 45);
			animator->UpdateNodeSize(mcNodes.Get(i)->GetId(),20,20); // to change the node size in the animation.
		}
		for (uint32_t i = 0; i < scNodes.GetN(); ++i) {
			animator->UpdateNodeDescription(scNodes.Get(i), "SC " + std::to_string(i));
			animator->UpdateNodeColor(scNodes.Get(i), 0, 200, 200);
			animator->UpdateNodeSize(scNodes.Get(i)->GetId(),20,20); // to change the node size in the animation.
		}
		for (uint32_t i = 0; i < uavNodes.GetN(); ++i) {
			animator->UpdateNodeDescription(uavNodes.Get(i), "UAV " + std::to_string(i));
			animator->UpdateNodeColor(uavNodes.Get(i), 250, 200, 45);
			animator->UpdateNodeSize(uavNodes.Get(i)->GetId(),20,20); // to change the node size in the animation.
		}
		for (uint32_t j = 0; j < ueNodes.GetN(); ++j) {
			animator->UpdateNodeDescription(ueNodes.Get(j), "UE " + std::to_string(j));
			animator->UpdateNodeColor(ueNodes.Get(j), 20, 10, 145);
			animator->UpdateNodeSize(ueNodes.Get(j)->GetId(),20,20);
		}
		animator->UpdateNodeDescription(staticNodes.Get(0), "SGW");
		animator->UpdateNodeDescription(staticNodes.Get(1), "PGW");
		animator->UpdateNodeDescription(staticNodes.Get(2), "RemoteHost");
		for (uint32_t k = 0; k < staticNodes.GetN(); ++k) {
			animator->UpdateNodeColor(staticNodes.Get(k), 110, 150, 45);
			animator->UpdateNodeSize(staticNodes.Get(k)->GetId(),20,20);
		}
	}

	std::ofstream qos_vs_time;
	qos_vs_time.open("qos-vs-time.txt", std::ofstream::out);
	qos_vs_time << "time,id,data_type,delay,jitter,throughput,pdr" << std::endl;
	qos_vs_time.close();

	std::ofstream service_level;
	service_level.open("service-level.txt", std::ofstream::out);
	service_level << "time,id,served,bs_id,bs_type" << std::endl;
	service_level.close();

	Ptr<FlowMonitor> flowMonitor;
	FlowMonitorHelper flowHelper;
	flowHelper.Install(remoteHost);
	flowMonitor = flowHelper.Install(BSNodes);
	flowMonitor = flowHelper.Install(ueNodes);
	Simulator::Schedule(Seconds(simTime-0.001), NetworkStatsMonitor, &flowHelper, flowMonitor);

	// populate simulation with requests trace
	auto requests = populate_requests_trace();

	Simulator::Schedule(Seconds(MOBILITY_ENERGY_INTERVAL), &update_mobility_energy, uavNodes);
	Simulator::Schedule(Seconds(COMMS_ENERGY_INTERVAL), &update_comms_energy, uavNodes);
	Simulator::Schedule(management_interval, ThroughputMonitor, &flowHelper, flowMonitor); // recurrent
	//the remote host is used as a fallback if there are no UAVs available
	Simulator::Schedule(Seconds(2), &assign_tasks, ueNodes);
	Simulator::Schedule(management_interval, &just_a_monitor);                 // just a monitor

	if(algorithm == "random")
		Simulator::Schedule(management_interval, &power_on_all_drones);
	else if (algorithm != "no-uavs")
		Simulator::Schedule(management_interval, &UAVManager);

	/* handover reporting callbacks*/
  Config::Connect("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverStart",
                  MakeCallback(&NotifyHandoverStartEnb));
  Config::Connect("/NodeList/*/DeviceList/*/LteUeRrc/ConnectionEstablished",
                  MakeCallback(&NotifyConnectionEstablishedUe));
  Config::Connect("/NodeList/*/DeviceList/*/LteUeRrc/HandoverStart",
                  MakeCallback(&NotifyHandoverStartUe));
  Config::Connect("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverEndOk",
                  MakeCallback(&NotifyHandoverEndOkEnb));

  Config::Connect("/NodeList/*/DeviceList/*/LteUeRrc/HandoverEndOk",
                  MakeCallback(&NotifyHandoverEndOkUe));

  /* signal reporting callbacks */
  Config::Connect("/NodeList/*/DeviceList/*/ComponentCarrierMapUe/*/LteUePhy/"
                  "ReportUeMeasurements",
                  MakeCallback(&ReportUeMeasurementsCallback));
  Config::Connect("/NodeList/*/DeviceList/*/LteEnbRrc/RecvMeasurementReport",
                  MakeCallback(&RecvMeasurementReportCallback));
  // Config::Connect("/NodeList/*/DeviceList/*/LteUeRrc/PhySyncDetection",
  //                 MakeCallback(&PhySyncDetectionCallback));
  // Config::Connect("/NodeList/*/DeviceList/*/LteUeRrc/RadioLinkFailure",
  //                 MakeCallback(&RadioLinkFailureCallback));

  lteHelper->EnableTraces(); // enable all traces

	Ptr<RadioEnvironmentMapHelper> remHelper = CreateObject<RadioEnvironmentMapHelper> ();
	if (remMode > 0){
		Ptr<SpectrumChannel> dlChannel = lteHelper->GetDownlinkSpectrumChannel ();
		uint32_t dlChannelId = dlChannel->GetId ();
		NS_LOG_INFO ("DL ChannelId: " << dlChannelId);
		remHelper->SetAttribute ("Channel", PointerValue (dlChannel));
		remHelper->SetAttribute ("XMin", DoubleValue (0.0));
		remHelper->SetAttribute ("XMax", DoubleValue (3000.0));
		remHelper->SetAttribute ("XRes", UintegerValue (1000));
		remHelper->SetAttribute ("YMin", DoubleValue (0.0));
		remHelper->SetAttribute ("YMax", DoubleValue (3000.0));
		remHelper->SetAttribute ("YRes", UintegerValue (1000));
		remHelper->SetAttribute ("Z", DoubleValue (1.0));
		remHelper->SetAttribute ("Bandwidth", UintegerValue (UAV_BW));
		remHelper->SetAttribute ("Earfcn", UintegerValue (100));

		if (remRbId >= 0)
        {
          remHelper->SetAttribute ("UseDataChannel", BooleanValue (true));
          remHelper->SetAttribute ("RbId", IntegerValue (remRbId));
        }

		if(remMode == 1){
			remHelper->SetAttribute ("OutputFile", StringValue ("rem-start.out"));
			Simulator::Schedule (Seconds (15.0),&RadioEnvironmentMapHelper::Install,remHelper);
		} else {
			remHelper->SetAttribute ("StopWhenDone", BooleanValue (false));
			remHelper->SetAttribute ("OutputFile", StringValue ("rem-end.out"));
			Simulator::Schedule (Seconds (simTime-0.06),&RadioEnvironmentMapHelper::Install,remHelper);
		}
	}

	Simulator::Stop(Seconds(simTime));
	Simulator::Run();

	Simulator::Destroy();
	return 0;
}
