import argparse
import numpy as np
import pandas as pd
import sklearn.cluster as cluster
import time
import hdbscan

def create_clusters(data, algoritm, args, kwds):
    start_time = time.time()
    fitted_algoritm = algoritm(*args, **kwds).fit(data)
    end_time = time.time()
    print('Clustering took {:.2f} s'.format(end_time - start_time))
    return fitted_algoritm

def get_clusters(data, fitted_algoritm):
    n_clusters_ = len(set(fitted_algoritm.labels_)) - (1 if -1 in fitted_algoritm.labels_ else 0)
    print(fitted_algoritm.labels_)
    data = np.array(data)
    clusters = [data[fitted_algoritm.labels_ == i] for i in range(n_clusters_)]
    return clusters

def calculate_centroid(cluster):
    n_positions = int(cluster.size/3)
    sums = np.sum(cluster,axis=0)
    centroid = np.divide(sums, n_positions)
    return centroid

def calc_distances(point, positions):
    #numpy.linalg.norm(a - b) is equivalent to the euclidean distance
    #between the vectors a and b
    sub = point - positions
    distances = sub.apply(np.linalg.norm, axis=1)
    return distances

def format_result(uav_id, new_position, users):
    new_position_str = [str(coord) for coord in new_position]
    new_position_str = " ".join(new_position_str)

    users_str = [str(user) for user in users]
    users_str = " ".join(users_str)

    return f"{uav_id};{new_position_str};{users_str}"

parser = argparse.ArgumentParser(description='Cluster position data with different algoritms.')
parser.add_argument('-c', '--clusters', default=3,
                    help='Number of clusters to create. This option is only used by kmeans')
parser.add_argument('algo', metavar='algoritm',
                    help='Clustering algoritm to use',
                    choices=['kmeans', 'meanshift', 'dbscan', 'hdbscan'])
args = parser.parse_args()

user_data = pd.read_csv("user_status.txt", sep=' ',
    names=['id','x', 'y', 'z', 'priority'])
if user_data.empty:
    exit()

uav_data = pd.read_csv("uav-status.txt", sep=' ',
    names=['time', 'id', 'x', 'y', 'energy'])
if uav_data.empty:
    exit()
current_time = uav_data.iloc[-1]['time']
uav_data = uav_data[uav_data['time'] == current_time]
n_uavs = len(uav_data)

if args.algo == 'kmeans' or args.algo == 'meanshift':
    if args.algo == 'kmeans':
        fitted_algoritm = create_clusters(user_data[['x', 'y']],cluster.KMeans,(),
                                        {'n_clusters':int(args.clusters),'n_init':'auto'})
    else:
        fitted_algoritm = create_clusters(user_data[['x', 'y']], cluster.MeanShift, (), {})
    cluster_centers = np.array(fitted_algoritm.cluster_centers_)

else:
    if args.algo == 'dbscan':
        fitted_algoritm = create_clusters(user_data[['x', 'y']], cluster.DBSCAN, (), {'eps':100})
    else:
        fitted_algoritm = create_clusters(user_data[['x', 'y']], hdbscan.HDBSCAN, (), {'min_cluster_size': 15})
    clusters = get_clusters(user_data[['x', 'y']],fitted_algoritm)
    cluster_centers = np.array([calculate_centroid(cluster) for cluster in clusters])
centers = pd.DataFrame(cluster_centers, columns=['x','y'])
user_data['cluster'] = fitted_algoritm.labels_

available_id = [True] * n_uavs
results = []
for row in centers.itertuples():
    center = [row.x, row.y]
    available = uav_data[available_id]
    if len(available) == 1:
        best_id = available['id'].iloc[0]
    else:
        uav_pos = available[['x','y']]
        distances = calc_distances(center, uav_pos)
        best = distances.values.argmin()
        best_id = int(available['id'].iloc[best])
    users = user_data.loc[user_data['cluster'] == row.Index, 'id'].tolist()
    available_id[best_id] = False
    results.append(format_result(best_id, center, users))

with open("centers.txt", "w") as c:
    for res in results:
        c.write(f"{res}\n")
