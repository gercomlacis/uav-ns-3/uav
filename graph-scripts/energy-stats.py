#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
import glob
from collections import namedtuple

def getScenarios(path):
	scenarios = dict()

	directories = glob.glob(path + "/numUEs=*")
	directories.sort()
	for directory in directories:
		subdir = directory.split('/')[-1]
		numUEs = int(subdir.split('=')[-1])
		scenarios[numUEs] = directory

	return scenarios

def getNumUAVs(filePath):
	result = pd.read_csv(filePath, sep=' ', names=['time', 'id', 'used', 'remaining'], usecols=['id'])
	numUAVs = result.nunique()[0]
	return numUAVs

def getResult(filePath, numUAVs):
	result = pd.read_csv(filePath, sep=' ', names=['time', 'id', 'used', 'remaining'], usecols=['used'])
	total = result['used'].sum()
	if total > 1e12:
		#Compute energy was calculated incorrectly. Fix that by multiplying by 10^-28.
		total = total * 1e-28
	result = total / (3500 * numUAVs)

	return result

def getData(scenarios):
	data = dict()
	for scenario in scenarios.items():
		runs = glob.glob(f"{scenario[1]}/*/mobility-energy.txt")
		numUAVs = getNumUAVs(runs[0])
		traces = []
		for trace_file in trace_files:
			runs = glob.glob(f"{scenario[1]}/*/{trace_file}-energy.txt")
			qos = []
			for run in runs:
				qos_run = getResult(run, numUAVs)
				qos.append(qos_run)

			if runs == []:
				print("scenario " + scenario[1] + " has no data")
			else:
				print("scenario {} has {} data points".format(scenario[1], len(runs)))
				qos = pd.Series(qos)
				d = {'mean' : qos.mean(), 'std' : qos.std()}
				traces.append(d)
		data[scenario[0]] = pd.DataFrame(traces, index=trace_types)
	if len(data) > 0:
		data = pd.concat(data)

	print("")
	return data

trace_files = ['comms', 'compute', 'mobility']
trace_types = ['tx/rx', 'proc', 'fly']

ScenarioVariant = namedtuple("ScenarioVariant", ["name", "path"])

scenario_variants = [ScenarioVariant("Ti", "../results/numENBs=1/algo=random"),
		     ScenarioVariant("Tang", "../results/numENBs=1/algo=kmeans"),
#		     ScenarioVariant("TOPSIS", "../results/numENBs=1/algo=topsis"),
		     ScenarioVariant("UFECS", "../results/numENBs=0/algo=topsis")]

fig = plt.figure()
ax = fig.add_subplot(111)

width = 0.5       # the width of the bars

data_variants = dict()
for variant in scenario_variants:
	scenarios = getScenarios(variant.path)
	data_scenarios = getData(scenarios)
	if len(data_scenarios) == 0:
		continue
	data_variants[variant.name] = data_scenarios
data_variants = pd.concat(data_variants)

for numUEs in scenarios.keys():
	data_scenarios = data_variants.xs(numUEs, level=1)

	data_scenarios.reset_index(level=0, inplace=True, names='algo')
	Efly = data_scenarios.loc['fly']['mean']
	Eproc = data_scenarios.loc['proc']['mean']
	EtxErx = data_scenarios.loc['tx/rx']['mean']

	base1 = Efly.tolist()
	base2 = np.add(base1, Eproc).tolist()

	flyStd = data_scenarios.loc['fly']['std']
	procStd = data_scenarios.loc['proc']['std']
	txrxStd = data_scenarios.loc['tx/rx']['std']

	labels = data_scenarios.loc['fly']['algo'].tolist()
	p1 = ax.bar(labels, Efly, 1.25*width, yerr=flyStd, align='center', alpha=1.0, ecolor='black', capsize=6, color='#004c6d', linewidth=1.0, edgecolor='black')
	p2 = ax.bar(labels, Eproc, 1.25*width, bottom=base1, yerr=procStd, align='center', alpha=1.0, ecolor='black', capsize=6, color='#509c41', linewidth=1.0, edgecolor='black')
	p3 = ax.bar(labels, EtxErx, 1.25*width, bottom=base2, yerr=txrxStd, align='center', alpha=1.0, ecolor='black', capsize=6, color='#515354', linewidth=1.0, edgecolor='black')

	ax.legend( (p3[0], p2[0], p1[0]), trace_types, loc='upper center',
		bbox_to_anchor=(0., 1.12, 0.95, .102), ncol=3, borderaxespad=0.,  prop={'size': 14})

	ax.set_ylabel('Energy Consumption (%)', fontsize=17)
	ax.tick_params(axis="x", labelsize=17)
	ax.set_yticks(np.arange(0, 35.01, 5))
	ax.tick_params(axis="y", labelsize=15)
	ax.yaxis.grid(color='gray', linestyle='dotted')

	fig.tight_layout()
	fig.savefig(f"energy-stats-{numUEs}-users.png", format='png', dpi=300, bbox_inches = "tight")
	fig.savefig(f"energy-stats-{numUEs}-users.pdf", format='pdf', dpi=300, bbox_inches = "tight")
	#plt.show()

	ax.clear()
