#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
import glob
from collections import namedtuple

def getScenarios(path):
	scenarios = dict()

	directories = glob.glob(path + "/numUEs=*")
	directories.sort()
	for directory in directories:
		subdir = directory.split('/')[-1]
		numUEs = int(subdir.split('=')[-1])
		scenarios[numUEs] = directory

	return scenarios


def readFile(filePath):
	result = pd.read_csv(filePath, sep=',', names=['peers', 'pdr', 'plr', 'delay',
	'jitter', 'throughput', 'data_type'], usecols=['pdr', 'delay', 'jitter', 'data_type'])
	result = result.groupby('data_type').mean()
	result['delay'] = result['delay'] * 1000
	result['jitter'] = result['jitter'] * 1000

	return result

def getData(scenarios):
	data = dict()
	for scenario in scenarios.items():
		runs = glob.glob(scenario[1] + "/*/qos.txt")
		qos = []
		for run in runs:
			qos_run = readFile(run)
			qos.append(qos_run)

		if runs == []:
			print("scenario " + scenario[1] + " has no data")
		else:
			print("scenario {} has {} data points".format(scenario[1], len(runs)))
			qos = pd.concat(qos)
			qos = qos.groupby(level=0)
			d = {'mean' : qos.mean(), 'std' : qos.std()}
			data[scenario[0]] = pd.concat(d)
	if len(data) > 0:
		data = pd.concat(data)

	print("")
	return data

ScenarioVariant = namedtuple("ScenarioVariant", ["name", "path", "color"])

scenario_variants = [ScenarioVariant("No UAVs", "../results/numENBs=1/algo=no-uavs", "#ffc0cb"),
		     ScenarioVariant("Random", "../results/numENBs=1/algo=random", "#f4442d"),
		     ScenarioVariant("Kmeans", "../results/numENBs=1/algo=kmeans", "#992b1c"),
		     ScenarioVariant("TOPSIS", "../results/numENBs=1/algo=topsis", "#c1e7ff"),
		     ScenarioVariant("TOPSIS-no-enbs", "../results/numENBs=0/algo=topsis", "#346888")]

fig_pdr = plt.figure()
ax_pdr = fig_pdr.subplots()

fig_delay = plt.figure()
ax_delay = fig_delay.subplots()

fig_jitter = plt.figure()
ax_jitter = fig_jitter.subplots()

width = 0.15       # the width of the bars

data_variants = dict()
for variant in scenario_variants:
	scenarios = getScenarios(variant.path)
	data_scenarios = getData(scenarios)
	if len(data_scenarios) == 0:
		continue
	data_variants[variant.name] = data_scenarios
data_variants = pd.concat(data_variants)

for numUEs in scenarios.keys():
	variantN = 0
	pdr_rectangles = []
	delay_rectangles = []
	jitter_rectangles = []

	data_scenarios = data_variants.xs(numUEs, level=1)
	with_data = data_scenarios.index.get_level_values(0).unique()
	scale = (len(with_data)-1)/2
	for variant in scenario_variants:
		if variant.name not in with_data:
			continue
		data_scenario = data_scenarios.xs(variant.name)
		data_types = data_scenario.loc['mean'].index.array
		ind = np.arange(len(data_types))

		pdr = data_scenario['pdr']
		pdr_mean = pdr.loc['mean']
		pdr_std = pdr.loc['std']
		rect = ax_pdr.bar(ind + variantN*width, pdr_mean, width,
				yerr=pdr_std, align='center', alpha=1.0, ecolor='black', capsize=6, color=variant.color, linewidth=1.0, edgecolor='black')
		pdr_rectangles.append(rect[0])
		ax_pdr.set(xticks=ind+width*scale, xticklabels=data_types)
		ax_pdr.set_xlabel("Service type", fontsize=17)
		ax_pdr.tick_params(axis="x", labelsize=15)
		ax_pdr.set_yticks(np.arange(0,101,10))
		ax_pdr.set_ylabel("PDR (%)", fontsize=17)
		ax_pdr.tick_params(axis="y", labelsize=15)
		ax_pdr.yaxis.grid(color='gray', linestyle='dotted')

		delay = data_scenario['delay']
		delay_mean = delay.loc['mean']
		delay_std = delay.loc['std']
		rect = ax_delay.bar(ind + variantN*width, delay_mean, width, yerr=delay_std, align='center', alpha=1.0, ecolor='black', capsize=6, color=variant.color, linewidth=1.0, edgecolor='black')
		delay_rectangles.append(rect[0])
		ax_delay.set(xticks=ind+width*scale, xticklabels=data_types)
		ax_delay.set_xlabel("Service type", fontsize=17)
		ax_delay.tick_params(axis="x", labelsize=15)
		ax_delay.set_yticks(np.arange(0,121,10))
		ax_delay.set_ylabel("Delay (ms)", fontsize=17)
		ax_delay.tick_params(axis="y", labelsize=15)
		ax_delay.yaxis.grid(color='gray', linestyle='dotted')

		jitter = data_scenario['jitter']
		jitter_mean = jitter.loc['mean']
		jitter_std = jitter.loc['std']
		rect = ax_jitter.bar(ind + variantN*width, jitter_mean, width, yerr=jitter_std, align='center', alpha=1.0, ecolor='black', capsize=6, color=variant.color, linewidth=1.0, edgecolor='black')
		jitter_rectangles.append(rect[0])
		ax_jitter.set(xticks=ind+width*scale, xticklabels=data_types)
		ax_jitter.set_xlabel("Service type", fontsize=17)
		ax_jitter.tick_params(axis="x", labelsize=15)
		ax_jitter.set_yticks(np.arange(0,26,5))
		ax_jitter.set_ylabel("Jitter (ms)", fontsize=17)
		ax_jitter.tick_params(axis="y", labelsize=15)
		ax_jitter.yaxis.grid(color='gray', linestyle='dotted')

		variantN += 1

	labels = [variant.name for variant in scenario_variants]
	ax_pdr.legend(pdr_rectangles, labels, loc='upper center', bbox_to_anchor=(0., 1.12, 0.95, .102),
			ncol=3, borderaxespad=0.,  prop={'size': 14})
	ax_delay.legend(delay_rectangles, labels, loc='upper center', bbox_to_anchor=(0., 1.12, 0.95, .102),
			ncol=3, borderaxespad=0.,  prop={'size': 14})
	ax_jitter.legend(jitter_rectangles, labels, loc='upper center', bbox_to_anchor=(0., 1.12, 0.95, .102),
			ncol=3, borderaxespad=0.,  prop={'size': 14})

	fig_pdr.savefig("pdr-vs-tasks-{}-users.png".format(numUEs), format='png', dpi=300, bbox_inches = "tight")
	fig_pdr.savefig("pdr-vs-tasks-{}-users.pdf".format(numUEs), format='pdf', dpi=300, bbox_inches = "tight")
	fig_delay.savefig("delay-vs-tasks-{}-users.png".format(numUEs), format='png', dpi=300, bbox_inches = "tight")
	fig_delay.savefig("delay-vs-tasks-{}-users.pdf".format(numUEs), format='pdf', dpi=300, bbox_inches = "tight")
	fig_jitter.savefig("jitter-vs-tasks-{}-users.png".format(numUEs), format='png', dpi=300, bbox_inches = "tight")
	fig_jitter.savefig("jitter-vs-tasks-{}-users.pdf".format(numUEs), format='pdf', dpi=300, bbox_inches = "tight")
	#plt.show()

	ax_pdr.clear()
	ax_delay.clear()
	ax_jitter.clear()
