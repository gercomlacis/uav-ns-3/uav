#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
import glob
from collections import namedtuple

def getScenarios(path):
	scenarios = dict()

	directories = glob.glob(path + "/numUEs=*")
	directories.sort()
	for directory in directories:
		subdir = directory.split('/')[-1]
		numUEs = int(subdir.split('=')[-1])
		scenarios[numUEs] = directory

	return scenarios

def getNumUAVs(filePath):
	result = pd.read_csv(filePath, sep=' ', names=['time', 'id', 'used', 'remaining'], usecols=['id'])
	numUAVs = result.nunique()[0]
	return numUAVs

def getResult(filePath, numUAVs):
	result = pd.read_csv(filePath, sep=' ', names=['time', 'id', 'used', 'remaining'], usecols=['used'])
	total = result['used'].sum()
	if total > 1e12:
		#Compute energy was calculated incorrectly. Fix that by multiplying by 10^-28.
		total = total * 1e-28
	result = total / (3500 * numUAVs)

	return result

def getData(scenarios):
	data = []
	for scenario in scenarios.items():
		runs = glob.glob(f"{scenario[1]}/*/mobility-energy.txt")
		numUAVs = getNumUAVs(runs[0])
		traces = []
		for trace_file in trace_files:
			runs = glob.glob(f"{scenario[1]}/*/{trace_file}-energy.txt")
			energy = [getResult(run, numUAVs) for run in runs]
			energy = pd.Series(energy)
			traces.append(energy)

		if runs == []:
			print("scenario " + scenario[1] + " has no data")
			d = {'mean' : 0, 'std' : 0}
		else:
			print("scenario {} has {} data points".format(scenario[1], len(runs)))
			total_energy = pd.DataFrame(traces).sum()
			d = {'mean' : total_energy.mean(), 'std' : total_energy.std()}
		energy_result = pd.DataFrame(d, index=[scenario[0]])

		data.append(energy_result)
	data = pd.concat(data)
	print("")
	return data

trace_files = ['comms', 'compute', 'mobility']

ScenarioVariant = namedtuple("ScenarioVariant", ["name", "path", "color"])

scenario_variants = [ScenarioVariant("Ti", "../results/numENBs=1/algo=random", "#f4442d"),
		     ScenarioVariant("Tang", "../results/numENBs=1/algo=kmeans", "#992b1c"),
#		     ScenarioVariant("TOPSIS", "../results/numENBs=1/algo=topsis", "#c1e7ff"),
		     ScenarioVariant("UFECS", "../results/numENBs=0/algo=topsis", "#346888")]

fig = plt.figure()
ax = fig.subplots()

ind = np.arange(3)
width = 0.15       # the width of the bars
legend_cols = 4
variantN = 0
scale = (len(scenario_variants)-1)/2

rectangles = []

for variant in scenario_variants:
	scenarios = getScenarios(variant.path)
	data_scenarios = getData(scenarios)

	energy_mean = data_scenarios['mean']
	energy_std = data_scenarios['std']
	rect = ax.bar(ind + variantN*width, energy_mean, width, yerr=energy_std, align='center', alpha=1.0, ecolor='black', capsize=6, color=variant.color, linewidth=1.0, edgecolor='black')
	rectangles.append(rect[0])
	ax.set(xticks=ind+width*scale, xticklabels=list(scenarios.keys()))
	ax.set_xlabel("Number of users", fontsize=17)
	ax.tick_params(axis="x", labelsize=15)
	ax.set_yticks(np.arange(0,35.01,5))
	ax.set_ylabel("Energy Consumption (%)", fontsize=17)
	ax.tick_params(axis="y", labelsize=15)
	ax.yaxis.grid(color='gray', linestyle='dotted')

	variantN += 1

labels = [variant.name for variant in scenario_variants]
ax.legend(rectangles, labels, loc='upper center', bbox_to_anchor=(0., 1.04, 0.95, .102),
	    ncol=legend_cols, borderaxespad=0.,  prop={'size': 14})

fig.savefig("energy-total.png", format='png', dpi=300, bbox_inches = "tight")
fig.savefig("energy-total.pdf", format='pdf', dpi=300, bbox_inches = "tight")
#plt.show()
