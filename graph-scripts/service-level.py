#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
import glob
from collections import namedtuple

def getScenarioParameters(directory):
	parameters = dict()
	subdir = directory.split('/')
	for s in subdir:
		if s.find('=') != -1:
			p = s.split('=')
			if p[1].isnumeric():
				parameters[p[0]] = int(p[1])
			else:
				parameters[p[0]] = p[1]
	parameters["path"] = directory
	return parameters

def getScenarios(path):
	directories = glob.glob(path + "/numUEs=50")
	scenarios = [getScenarioParameters(d) for d in directories]
	scenarios.sort(key=lambda s: s["numUEs"])

	return scenarios

def readFile(filePath):
	result = pd.read_csv(filePath, sep=',', header=0,
				usecols=['time', 'served', 'bs_id', 'bs_type'])
	user_distribution = pd.DataFrame()
	user_distribution['time'] = result['time'].unique()

	served = result[result['served']==1]
	by_uavs = served[served['bs_type']=='uav'].groupby('time', as_index=False).size()
	by_uavs.rename(columns={'size': 'by_uavs'}, inplace=True)
	user_distribution = user_distribution.merge(by_uavs, how='left', on='time')

	by_bs = served[served['bs_type']=='bs'].groupby('time', as_index=False).size()
	by_bs.rename(columns={'size': 'by_bs'}, inplace=True)
	user_distribution = user_distribution.merge(
							by_bs, how='left', on='time', sort=True)
	user_distribution.fillna(0, inplace=True)

	return user_distribution

def getData(scenarios):
	data = []
	for scenario in scenarios:
		#Only successful simulations generate this file
		runs = glob.glob(scenario["path"] + "/*/qos.txt")
		runs = [r.removesuffix("qos.txt") + "service-level.txt" for r in runs]
		qos = []
		for run in runs:
			qos_run = readFile(run)
			qos_run[['by_bs', 'by_uavs']] = qos_run[['by_bs', 'by_uavs']] * 100 / scenario['numUEs']
			qos.append(qos_run)

		if runs == []:
			print("scenario {} has no data".format(scenario["path"]))
			zeroes = pd.DataFrame(data=[0, 0, 0], columns=['time',
				'unserved','by_uavs', 'by_bs'])
			d = {'mean' : zeroes, 'std' : zeroes}
		else:
			print("scenario {} has {} data points".format(scenario["path"], len(runs)))
			qos = pd.concat(qos)
			d = {'mean' : qos.groupby('time', as_index=False).mean(), 'std' : qos.groupby('time', as_index=False).std()}
		qos_result = pd.concat(d)

		data.append(qos_result)
	data = pd.concat(data)
	return data

RESULT_PATH = "../results"

ScenarioVariant = namedtuple("ScenarioVariant", ["name", "path", "color"])

scenario_variants = [ScenarioVariant("Random",
				RESULT_PATH + "/algo=random/numUAVs=12", "lightpink"),
					 ScenarioVariant("Tang",
				RESULT_PATH + "/algo=kmeans/numUAVs=12", "darkred"),
					 ScenarioVariant("Flyed",
				RESULT_PATH + "/algo=topsis/numUAVs=12", "steelblue"),
					 ScenarioVariant("Coop",
				RESULT_PATH + "/algo=cooperative/numUAVs=12", "blue")]
#		     ScenarioVariant("UFECS 25 uavs", "../results/numENBs=1/algo=topsis/numUAVs=25", "#346888")]

ind = np.arange(1)
width = 0.15       # the width of the bars
legend_cols = 4
variantN = 1
total_variants = len(scenario_variants)

lines = []
fig_service_level = plt.figure()
for variant in scenario_variants:
	ax_service_level = fig_service_level.add_subplot(total_variants, 1, variantN)

	scenarios = getScenarios(variant.path)
	data_scenarios = getData(scenarios)

	mean = data_scenarios.loc['mean']
	l = ax_service_level.stackplot(mean['time'], mean[['by_bs', 'by_uavs']].T,
			labels=['MEC', variant.name], colors=['green', variant.color], alpha=0.8)
	#ax_service_level.tick_params(axis="x", labelsize=15)
	ax_service_level.set_yticks(np.arange(0,101,25))
	ax_service_level.yaxis.grid(color='gray', linestyle='dotted')
	#ax_service_level.tick_params(axis="y", labelsize=15)
	if variantN == 1:
		lines.extend(l)
	else:
		lines.append(l[1])
	served = mean['by_bs'].iloc[-1] + mean['by_uavs'].iloc[-1]
	print(f"{variant.name}: {served}%\n")

	variantN += 1

names = [s.name for s in scenario_variants]
names.insert(0, "MEC")

fig_service_level.supxlabel("Simulation Time(s)", fontsize=17)
fig_service_level.supylabel("SPR(%)", fontsize=17)
fig_service_level.legend(lines, names, loc='upper center',
		bbox_to_anchor=(0.5,1.12),
	ncol=legend_cols, borderaxespad=0.2,  prop={'size': 14})
fig_service_level.tight_layout()

fig_service_level.savefig("service-level-vs-time.png", format='png', dpi=300, bbox_inches = "tight")
fig_service_level.savefig("service-level-vs-time.pdf", format='pdf', dpi=300, bbox_inches = "tight")
#plt.show()
