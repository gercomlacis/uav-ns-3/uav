#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import csv
import glob
from collections import namedtuple

def getScenarios(path):
	scenarios = dict()

	directories = glob.glob(path + "/numUEs=*")
	directories.sort()
	for directory in directories:
		subdir = directory.split('/')[-1]
		numUEs = int(subdir.split('=')[-1])
		scenarios[numUEs] = directory

	return scenarios


def processFile(filePath):
	success_rate = dict()
	result = pd.read_csv(filePath, sep=',', names=['peers', 'pdr', 'plr', 'delay',
	'jitter', 'throughput', 'data_type'], usecols=['delay', 'data_type'])
	grouped = result.groupby('data_type')
	for name, group in grouped:
		successful = len(group[group['delay'] < service_max_delay[name]])
		total = len(group)
		success_rate[name] = (successful / total) * 100

	return pd.Series(success_rate)

def getData(scenarios):
	data = dict()
	for scenario in scenarios.items():
		runs = glob.glob(scenario[1] + "/*/qos.txt")
		all_runs_data = []
		for run in runs:
			run_data = processFile(run)
			all_runs_data.append(run_data)

		if runs == []:
			print("scenario " + scenario[1] + " has no data")
		else:
			print("scenario {} has {} data points".format(scenario[1], len(runs)))
			all_runs_data = pd.DataFrame(all_runs_data)
			d = {'mean' : all_runs_data.mean(), 'std' : all_runs_data.std()}
			data[scenario[0]] = pd.DataFrame.from_dict(d, orient='index')
	if len(data) > 0:
		data = pd.concat(data)

	print("")
	return data

ScenarioVariant = namedtuple("ScenarioVariant", ["name", "path", "color"])

scenario_variants = [ScenarioVariant("Pandey", "../results/numENBs=1/algo=no-uavs", "#ffc0cb"),
				ScenarioVariant("Ti", "../results/numENBs=1/algo=random", "#f4442d"),
				ScenarioVariant("Tang", "../results/numENBs=1/algo=kmeans", "#992b1c"),
#				ScenarioVariant("TOPSIS", "../results/numENBs=1/algo=topsis", "#c1e7ff"),
				ScenarioVariant("UFECS", "../results/numENBs=0/algo=topsis", "#346888")]

service_max_delay = {"REALTIME" : 0.1,
					"IMAGE" : 1.0,
					"VIDEO" : 0.25,
					"CONTAINER" : 0.15}

fig_delay = plt.figure()
ax_delay = fig_delay.subplots()

width = 0.15       # the width of the bars
legend_cols = 2

data_variants = dict()
for variant in scenario_variants:
	scenarios = getScenarios(variant.path)
	data_scenarios = getData(scenarios)
	if len(data_scenarios) == 0:
		continue
	data_variants[variant.name] = data_scenarios
data_variants = pd.concat(data_variants)

for numUEs in scenarios.keys():
	variantN = 0
	pdr_rectangles = []
	delay_rectangles = []
	jitter_rectangles = []

	data_scenarios = data_variants.xs(numUEs, level=1)
	with_data = data_scenarios.index.get_level_values(0).unique()
	scale = (len(with_data)-1)/2
	for variant in scenario_variants:
		if variant.name not in with_data:
			continue
		data_scenario = data_scenarios.xs(variant.name)
		data_types = data_scenario.loc['mean'].index.array
		ind = np.arange(len(data_types))

		delay_mean = data_scenario.loc['mean']
		delay_std = data_scenario.loc['std']
		rect = ax_delay.bar(ind + variantN*width, delay_mean, width, yerr=delay_std, align='center', alpha=1.0, ecolor='black', capsize=6, color=variant.color, linewidth=1.0, edgecolor='black')
		delay_rectangles.append(rect[0])
		ax_delay.set(xticks=ind+width*scale, xticklabels=data_types)
		ax_delay.set_xlabel("Service type", fontsize=17)
		ax_delay.tick_params(axis="x", labelsize=15)
		ax_delay.set_yticks(np.arange(0,101,10))
		ax_delay.set_ylabel("Success Rate (%)", fontsize=17)
		ax_delay.tick_params(axis="y", labelsize=15)
		ax_delay.yaxis.grid(color='gray', linestyle='dotted')

		variantN += 1

	labels = [variant.name for variant in scenario_variants]
	ax_delay.legend(delay_rectangles, labels, loc='upper center', bbox_to_anchor=(0., 1.12, 0.95, .102),
			ncol=legend_cols, borderaxespad=0.,  prop={'size': 14})

	fig_delay.savefig("service-stats-{}-users.png".format(numUEs), format='png', dpi=300, bbox_inches = "tight")
	fig_delay.savefig("service-stats-{}-users.pdf".format(numUEs), format='pdf', dpi=300, bbox_inches = "tight")
	#plt.show()

	ax_delay.clear()
